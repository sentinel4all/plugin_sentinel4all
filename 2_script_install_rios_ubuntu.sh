# Download and instal RIOS
wget https://github.com/ubarsc/rios/releases/download/rios-1.4.10/rios-1.4.10.tar.gz && \
		 tar -xzf rios-1.4.10.tar.gz && \
		 mkdir RIOSInstall && \
		 cd rios-1.4.10 && \
		 python3 setup.py install --prefix=../RIOSInstall && \
		 mv ../RIOSInstall /usr/local/lib

# Add env var
touch /etc/profile.d/AddPath.sh  && \
chmod +x /etc/profile.d/AddPath.sh && \
cd -
echo "if [ -d \"/usr/local/lib/RIOSInstall/bin\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    PATH=\"\$PATH:/usr/local/lib/RIOSInstall/bin\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh
# Check and change if needed the python version 
echo "if [ -d \"/usr/local/lib/RIOSInstall/lib/python3.10/site-packages\" ] ; then" >> /etc/profile.d/AddPath.sh && \
# Check and change if needed the python version 
echo "    export PYTHONPATH=\"/usr/local/lib/RIOSInstall/lib/python3.10/site-packages\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh
