| <img src="./Pictures/ONFi_logotype_A_pantone.png" alt="drawing" height="60"/> | <img src="./Pictures/logo_lastig.png" alt="drawing" height="65"/>                 <img src="./Pictures/Logo_gustave_eiffel.jpg" alt="drawing" height="60"/> |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| clardeux@gmail.com<br />cedric.lardeux@onfinternational.com  |                 pierre-louis.frison@u-pem.fr                 |


# Plugin Sentinel4all

<u>**First Draft version**</u>

## Introduction

This plug-in works on QGIS. It aims to keep accessible the use of spaceborne Sentinel-1 and -2 data.

Indeed, even if Sentinel-1 and 2 data are freely available, their processing, especially for Sentinel-1, is not necessary easy to do for non specialist.

For this reason we developed this plug-in in order to provide generic tools allowing any people with GIS skills to be able to do some Sentinel preprocessing as well as supervised classification as illustrated by the following picture:

![](./Pictures/screenshot_processing_plugin.png)

This plugin is based on the following open source libs: 

​		**Orfeo ToolBox** https://www.orfeo-toolbox.org 

​		**RIOS** 

​		and **python libraries** used by the codes we developed

## how to install it ? 
- Windows install: https://gitlab.com/sentinel4all/kit_qgis_sentinel/-/blob/windows/README.md
- Ubuntu install: 
copy the folder Sentinel_for_all into ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/

## To do what ? 
### Sentinel-1 processing

- Because Sentinel-1 data are freely available but not delivered on a format fully usable as Sentinel-2 (ortorectified, ...) we develop a tools that get Sentinel-1 zip files (GRD format) and on shape file delimiting the area and that process all the data. The output data are calibrated and orthorectified.  <u>**Tutorial Link TO DO.**</u>
- In order to remove the speckle we devellop a tools that apply a temporal speckle filter. <u>**Tutorial Link TO DO.**</u>

### Classification

The classification tools is based on the Orfeo ToolBox and allow user to create a RandomForest classification

## The team

- Cédric Lardeux (ONF International,  http://www.onfinternational.org)
- Pierre-Louis Frison  (Gustave Eiffel University, https://igm.u-pem.fr/presentation/)

## FAQs





