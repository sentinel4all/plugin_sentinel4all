# -*- coding: utf-8 -*-
"""
S1OwnLibrary.pyProcessS1ZipDataset
Created on Tue Sep 26 18:13:52 2017

@author: cedric
"""

'''
Sentinel-1 own Library
'''
# import os, sys, subprocess, shutil, zipfile, logging
# from inspect import getsourcefile
# from datetime import datetime
# import concurrent.futures

# from osgeo import  ogr, osr, gdal
# import numpy as np
# from scipy.ndimage.filters import uniform_filter
# from random import shuffle

# from rios import applier
# from rios import cuiprogress

# from scipy.signal import argrelextrema
# from scipy.signal import medfilt
# from skimage.filters.rank import median

# import pandas as pd
import pdb

import os, re, sys, subprocess, shutil, zipfile, logging
from inspect import getsourcefile
from datetime import datetime
import concurrent.futures

from osgeo import ogr, osr, gdal
import numpy as np
from scipy.ndimage import uniform_filter
from random import shuffle

from rios import applier
from rios import cuiprogress

from scipy.signal import argrelextrema
from scipy.signal import medfilt
from skimage.filters.rank import median

import pandas as pd

from pathlib import Path

import tempfile


def mad(a, axis=None):
    """
    Compute *Median Absolute Deviation* of an array along given axis.
    """

    # Median along given axis, but *keeping* the reduced axis so that
    # result can still broadcast against a.
    med = np.median(a, axis=axis, keepdims=True)
    mad = np.median(np.absolute(a - med), axis=axis)  # MAD along given axis

    return mad

'''
This function the mean and stdev over rolling windows in 3d
It use a huge amount of ram
'''
def compute_window_mean_and_var_strided(image, window_b, window_w, window_h):
    image = np.lib.pad(image, ((window_b/2,window_b/2), (window_w/2,window_w/2),(window_h/2,window_h/2)), 'reflect')
    b, w, h = image.shape
    strided_image = np.lib.stride_tricks.as_strided(image, 
                                                    shape=[b - window_b +1 ,w - window_w + 1, h - window_h + 1, window_b, window_w, window_h],
                                                    strides=image.strides + image.strides)
    # important: trying to reshape image will create complete 4-dimensional compy
    means = strided_image.mean(axis=(3,4,5)) 
    mean_squares = (strided_image ** 2).mean(axis=(3, 4, 5)) 
    
    variations = mean_squares - means ** 2

    return means, np.sqrt(variations)

'''
Because compute_window_mean_and_var_strided use a lot of ram
this one use a loop over the first dimmension
It's slower but use less ram
'''
def ComputeMEanStdevLoopBand(aImage,WinTimeSize, WinSpatSize):
    b, r, c = aImage.shape
    
    # First b element
    Mean, Std = compute_window_mean_and_var_strided(aImage[0:WinTimeSize,:,:], WinTimeSize, WinSpatSize, WinSpatSize)
    Mean, Std = Mean[WinTimeSize / 2, :, :] , Std[WinTimeSize / 2, :, :]
    Mean, Std = Mean[np.newaxis, :, :], Std[np.newaxis, :, :]    
    
    for i in range(b -2):
        MeanTmp, StdTmp = compute_window_mean_and_var_strided(aImage[i+1:i+1+WinTimeSize,:,:], WinTimeSize, WinSpatSize, WinSpatSize)
        MeanTmp, StdTmp = MeanTmp[WinTimeSize / 2, :, :] , StdTmp[WinTimeSize / 2, :, :]
        MeanTmp, StdTmp = MeanTmp[np.newaxis, :, :], StdTmp[np.newaxis, :, :]
        Mean = np.concatenate((Mean,MeanTmp), axis=0 )
        Std = np.concatenate((Std,StdTmp), axis=0 )
    
    return Mean, Std


'''
Compute the ENL over 3D rolling window
'''
def ComputeENL(aData,aWinTimeSize, aWinSpatSize):
    Mean, STDV = ComputeMEanStdevLoopBand(aData,aWinTimeSize, aWinSpatSize)
    return np.sqrt(Mean /STDV)

def ComputeEntropy(aImage, aWinSize):
    NumDate, rows, cols = aImage.shape
    # Smooth to have a good estimator
    DataSmooth = uniform_filter(aImage, size=(0,aWinSize,aWinSize))
    
    # Create Numpy array
    Entropy = np.zeros(shape=(NumDate-1, rows, cols ),dtype=float)
    
    for i in range(NumDate-1):
        p1 = DataSmooth[i,:,:] /(DataSmooth[i,:,:] + DataSmooth[i+1,:,:])
        p2 = DataSmooth[i+1,:,:] /(DataSmooth[i,:,:] + DataSmooth[i+1,:,:])
        Entropy[i,:,:] = - (p1 * np.log2(p1) + p2 * np.log2(p2))
        
    return Entropy
        

'''
Compute successive stable time serie and report in 3d array one id for each stable part
'''
def GetStableIdPart(aENL,aENLThreshold,aFullDataShape):
    ENLChange = np.arange(np.prod(aFullDataShape) ).reshape(aFullDataShape) + 1
    for i in range(ENLChange.shape[0]-1):
        ENLi = aENL[i,:,:]

        ENLiPrevChange = ENLChange[i,:,:]
        ENLNewChange = ENLChange[i+1,:,:]

        ENLNewChange[ENLi >= aENLThreshold] = ENLiPrevChange[ENLi >= aENLThreshold]
        ENLChange[i+1,:,:] = ENLNewChange
        
    return ENLChange

'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")
    
    return joinedPath

'''
This function return the qgis script folder
'''
def getQGISProcessingScriptFolder():
    from qgis.core import QgsApplication
    QGISProcessingScriptFolder = os.path.dirname(QgsApplication.qgisSettingsDirPath())
    QGISProcessingScriptFolder = modOsJoinPath([QGISProcessingScriptFolder,
    'processing', 'scripts'])
    
    return QGISProcessingScriptFolder

'''
This function load the necessary libs in different way
if we are running script in qgis processing or not
'''
def LoadS1Lib():
    # Boolean to know if in qgis
    try:
        import qgis.utils
        inqgis = qgis.utils.iface is not None
    except ImportError:
        no_requests = True
        inqgis = False
    
    if inqgis:
        QGISProcessingScriptFolder = getQGISProcessingScriptFolder()
        
        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([QGISProcessingScriptFolder, 'S1Lib'])
    else:
        LocalScriptFileDir = os.path.dirname(os.path.abspath((getsourcefile(lambda:0)))).replace("\\","/")
        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([LocalScriptFileDir, 'S1Lib'])  

'''
Function that take a generic string of available input value and that
return the real OTB argument value (Calibration_Type) and its acronyme Calib_Name
'''
def ReturnRealCalibrationOTBValue(aCalibration_Type='Sigma0'):
    Calibration_Type = ''
    if aCalibration_Type == 'Sigma0':
        Calibration_Type = 'sigma'
        Calib_Name='Sig0'
    elif aCalibration_Type == 'Gamma0':
        Calibration_Type = 'gamma'
        Calib_Name='Gam0'
    elif aCalibration_Type == 'Beta0':
        Calibration_Type = 'beta'
        Calib_Name='Bet0'
    else:
        Calibration_Type='sigma'
        Calib_Name='Sig0'
    
    return Calibration_Type, Calib_Name
    
    
'''
This function list all file with a given file extention in one directory
'''
def GetFileByExtensionFromDirectory(directory = '/Dir/To/Scan', filt_ext = 'abs.safe'):
    list_file = []
    for root, dirnames, filenames in os.walk(directory, followlinks=True):
        for filename in filenames:
            if filename.endswith((filt_ext)):
                list_file.append(modOsJoinPath([root, filename]))

    list_file.sort()
    return list_file

'''
This function return the relative orbit of a raw S1 manifest file
'''
def getRelativeOrbit(aManifestFile):
    file = open(aManifestFile,"r")
    for line in file:
        if "<safe:relativeOrbitNumber type=\"start\">" in line:
            RelativeOrbit = int(line.replace("            <safe:relativeOrbitNumber type=\"start\">","").replace("</safe:relativeOrbitNumber>",""))
    file.close()
    return RelativeOrbit
   
'''
Extract the acquisition day from raw S1 tif file

'''
def getAcqDayFromRawS1TifFile(aFileName):
    aFileName = aFileName.split("/")[-1]
   
    return aFileName.split("_")[4][0:8]

def getDayFromS1FileOrFolder(aFileName):
    # get element date over 2000 year
    regex = r"(20\d{2})(\d{2})(\d{2})" 
    matches = re.finditer(regex, aFileName, re.MULTILINE)
    dates = list(set([i.group() for i in matches]))

    if dates:
        dates = dates[0]
    else:
        dates = None

    return dates

'''
Extract the acquisition day on processed data
'''
def getDateFromS1Raster(PathToRaster):
    return PathToRaster.split("/")[-1].split("-")[4][0:8]

'''
This function filter the manifest list file based on date extraction of already
processed data
'''
def GetNewDatesFromListFilesInputManifest(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = GetFileByExtensionFromDirectory(aOutputDir, 'tiff') +  GetFileByExtensionFromDirectory(aOutputDir, 'tif')
   
    # List all unique date in input file
    AllInputFiles = GetFileByExtensionFromDirectory(aInputDir, 'tiff') +  GetFileByExtensionFromDirectory(aInputDir, 'tif')

    ListDates = [getAcqDayFromRawS1TifFile(RastPath) for RastPath in AllOutputFiles ]
    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()
   
    ListDates = [getDateFromS1Raster(RastPath) for RastPath in AllInputFiles ]
    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()

    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    # Filter the data
    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item)

    return NewList

def GetNewDatesFromListFiles(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = GetFileByExtensionFromDirectory(aOutputDir, 'tiff') +  GetFileByExtensionFromDirectory(aOutputDir, 'tif')
    AllOutputFiles = [ file.replace("\\","/") for file in AllOutputFiles]
    # List all unique date in input file
    AllInputFiles = GetFileByExtensionFromDirectory(aInputDir, 'tiff') +  GetFileByExtensionFromDirectory(aInputDir, 'tif')
    AllInputFiles = [ file.replace("\\","/") for file in AllInputFiles if 'TempProcStack' not in file]


    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOutputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllInputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()


    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    print('NewDates',  NewDates)
    # Filter the data

    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item.replace("\\","/"))

    return NewList

'''
This function filter the zip  list file based on date extraction of already
processed data
'''
def GetNewDatesFromListZipFilesInput(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = GetFileByExtensionFromDirectory(aOutputDir, 'tiff') +  GetFileByExtensionFromDirectory(aOutputDir, 'tif')
   
    # List all unique date in input file
    AllInputFiles = GetFileByExtensionFromDirectory(aInputDir, 'zip')

    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOutputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllInputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()



    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    # Filter the data
    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item)

    return NewList

'''
This function get new date comparing orthorectification folder and Temporal filtering
folder.
'''
def GetNewDatesComparingOrthoFolderAndTempFiltFolder(aOrthoFolder, aTempFiltFolder):
    AllInputFiles = GetFileByExtensionFromDirectory(aOrthoFolder, 'tif')

    # Get all acquisition date
    ListInputDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllInputFiles if 'RasterArea' not in RastPath]
    # Remove None element if there is
    ListInputDates = [i for i in ListInputDates if i]
    
    # Get all unique dates
    UniqueInputDates = list(set(ListInputDates))
    UniqueInputDates.sort()
    
    # Get Dates from Output
    AllOutputFiles = GetFileByExtensionFromDirectory(aTempFiltFolder, 'tif')
    ListOutputDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOutputFiles if 'TempProcStack' not in RastPath]
    # Remove None element if there is
    ListOutputDates = [i for i in ListOutputDates if i]


    UniqueOutputDates = list(set(ListOutputDates))
    UniqueOutputDates.sort()

    # Check if prvious filtering
    BoolPrevFilt = False
    for afile in AllOutputFiles:
        if 'TempProcStack' in afile:
            BoolPrevFilt = True
    
    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    # If previous filtering we need to take the first old date
    if BoolPrevFilt and len(NewDates) > 0:
        FirstOldDate = UniqueInputDates[UniqueInputDates.index(NewDates[0]) -1]
        NewDates.append(FirstOldDate)
        NewDates.sort()
    
    return NewDates, BoolPrevFilt

def GetInputOutputListFilesForTempFiltering(aInput_Data_Folder,aOutput_Data_Folder,
                                        aNewDates,aOutput_in_dB, aTmpDirTempFilt,
                                        aWindow_Temp_Filtering):
    # Get input files to process
    AllTifFiles = GetFileByExtensionFromDirectory(aInput_Data_Folder, 'tif')
    # Dont take into account already filtered dates
    AllTifFile = []
    for NDate in aNewDates:
        for file in AllTifFiles:
            if NDate in file:
                AllTifFile.append(file)
    
    AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
    AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]
    
    if 'nt' in os.name:
        AllCopolFile = [file.replace("\\","/") for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
        AllCrosspolFile = [file.replace("\\","/") for file in AllTifFile if ('HV' in file) or  ('VH' in file)]
    else:
        AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
        AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]

    # Get the input list date to order them
    ListInputDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllCopolFile]
    # Remove None element if there is
    ListInputDates = [i for i in ListInputDates if i]


    # Get sorted index by date
    SortIndexInputDates = np.argsort(ListInputDates)
    AllCopolFile = [AllCopolFile[index] for index in SortIndexInputDates]

    AllCrosspolFile = [AllCrosspolFile[index] for index in SortIndexInputDates]

    AllTifFiles = GetFileByExtensionFromDirectory(aOutput_Data_Folder, 'tif')
    QueganFile = [ Queg for Queg in AllTifFiles if 'TempProcStack' in Queg]
    BoolPrevFilt = False
    # Begin output at first date if not prev filtering
    BeginDate = 0

    if len(QueganFile) ==4:
        # Begin outpur at first date if not prev filtering
        BeginDate = 1
        BoolPrevFilt = True
        for QFile in QueganFile:
            if ('VV' or 'HH') in QFile:
                if 'Count' in QFile:
                    CopolQueguanFileCountPart = QFile
                else:
                    CopolQueguanFileSumPart = QFile
            else:
                if 'Count' in QFile:
                    CrosspolQueguanFileCountPart = QFile
                else:
                    CrosspolQueguanFileSumPart = QFile
    else:
        CopolQueguanFileSumPart = ''
        CopolQueguanFileCountPart = ''
        CrosspolQueguanFileSumPart = ''
        CrosspolQueguanFileCountPart= '' 


    
    # Create Output list data
    AllOutCopolFile = []
    AllOutCrosspolFile = []
    
    if not aOutput_in_dB:
        aTmpDirTempFilt = aOutput_Data_Folder
    
    for CopolFile in AllCopolFile[BeginDate:]:
        # DirName of currentfile
        DirFile =  os.path.dirname(CopolFile)
        DirName = os.path.split(DirFile)[1]
    
        # Create Output subfolder
        OutFolder = modOsJoinPath([aTmpDirTempFilt,DirName])
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)
    
        FileName = os.path.basename(os.path.splitext(CopolFile)[0])
        TempFilterFileName = modOsJoinPath([OutFolder,FileName + '_TempFilt_W'\
        + str(aWindow_Temp_Filtering) + '.tif'])
        AllOutCopolFile.append(TempFilterFileName.replace("\\","/"))
    
       
    
    for CrosspolFile in AllCrosspolFile[BeginDate:]:
        # DirName of currentfile
        DirFile =  os.path.dirname(CrosspolFile)
        DirName = os.path.split(DirFile)[1]
    
        # Create Output subfolder
        OutFolder = modOsJoinPath([aTmpDirTempFilt,DirName])
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)
    
        FileName = os.path.basename(os.path.splitext(CrosspolFile)[0])
        TempFilterFileName = modOsJoinPath([OutFolder,FileName + '_TempFilt_W' \
        + str(aWindow_Temp_Filtering) + '.tif'])
        AllOutCrosspolFile.append(TempFilterFileName.replace("\\","/"))
        
    return AllCopolFile,AllOutCopolFile, AllCrosspolFile, AllOutCrosspolFile,\
        CopolQueguanFileSumPart, CopolQueguanFileCountPart, CrosspolQueguanFileSumPart, CrosspolQueguanFileCountPart
    
'''
This function apply teporal filtering over a 3d numpy array
'''
def QueganTemporalSpeckleFiltering(aTempArray, aSumPart, aPrevNumDates, aWinSize):
    eps = 1e-16
    # Jk = E[Ik]/N * Sum (I/E[I])
    NumTime, Rows, Cols = aTempArray.shape

    FiltTempArray = np.zeros(shape=(NumTime, Rows, Cols),dtype=float)
    # SumPart = np.zeros(shape=(Rows, Cols),dtype=float)

    for i in range(NumTime):
        aSumPart += aTempArray[i,:,:] / (uniform_filter(aTempArray[i,:,:], size=aWinSize) + eps )

    for i in range(NumTime):
        FiltTempArray[i,:,:] += uniform_filter(aTempArray[i,:,:], size=aWinSize) * aSumPart / (float(NumTime) + float(aPrevNumDates))

    return FiltTempArray, aSumPart


'''
This function apply teporal filtering over a 3d numpy array
'''
def QueganTemporalSpeckleFilteringRIOS(info, inputs, outputs, otherargs):
    eps = 1e-16
    NumTime = len(inputs.imgs) - 1
    
    # Read first image as sumpart just to create a 2D array due to RIOS
    aSumPart = inputs.imgs[0].astype(np.float32)
    
    # We remove the first band dimension equal to one
    aSumPart = aSumPart[0,:,:]
    
    # Replace all value to 0.
    aSumPart.fill(0.)
    
    # Get row, cols
    Rows, Cols = aSumPart.shape
    
    # List of numpy array for the output
    OutList = []
    
    # Test if previous filtering if yes we read the data
    if otherargs.PrevFilt:
#        print 'In quegan filtering func : YES previous filtering'
        aSumPart = inputs.imgs[NumTime].astype(np.float32)
        # We remove the first band dimension equal to one
        aSumPart = aSumPart[0,:,:]
#    else:
#        print 'In quegan filtering func : NO previous filtering'

    FiltTempArray = np.zeros(shape=(NumTime, Rows, Cols),dtype=float)
    
    for img in inputs.imgs[:-1]:
        # We remove the 1d band dimension due to RIOS
        img = img[0,:,:]
        aSumPart += img / ( uniform_filter(img, size=otherargs.WinSize) + eps )
        
    for i, img in enumerate(inputs.imgs[:-1]):
        # We remove the 1d band dimension due to RIOS
        img = img[0,:,:]
        FiltTempArray[i,:,:] += uniform_filter(img, size=otherargs.WinSize) \
        * aSumPart / (float(NumTime) + float(otherargs.NPrevDates))
    
        OutList.append(np.expand_dims(FiltTempArray[i,:,:], axis=0))
    OutList.append(np.expand_dims(aSumPart, axis=0))

    outputs.imgs = OutList


'''
This function apply teporal filtering over a 3d numpy array
'''
def QueganTemporalSpeckleFilteringRIOSV2(info, inputs, outputs, otherargs):
    # Test medfilt
    eps = 1e-16
	
    # To initiate
    aData = inputs.imgs[0]
    
    if otherargs.PrevFilt:
        WhNoise = inputs.imgs[-1][0,:,:] == 0
    else:
        WhNoise =  aData < 0.001
        Mask = aData
        Mask = Mask[0,:,:] * 0. + 1

        WhNoise =  aData[0,:,:] < 0.001
        Mask[ WhNoise ] = 0
    
        MaskMed = median(Mask, mask = Mask)
        WhNoise = MaskMed <= 0

    # We get data in np array
    for img in inputs.imgs[1:-2]:
        # We remove the 1d band dimension due to RIOS
        
        aData = np.concatenate((aData ,img), axis=0)
        
    
    #WhNoise = WhNoise[0,:,:]
    # WhNoise = np.logical_and(aData > 0,  aData < 0.001)
    # WhNoise =  aData < 0.001
    for i in range(aData.shape[0]):
        aData[i,:,:][WhNoise] = 0.001
        
    # aData[WhNoise] = 0.001
    # aData[WhNoise] = 0.001
    # Compute change part
    #ENL = ComputeENL(aData,otherargs.WinTimeSize, otherargs.WinSpatSize)
    

	
    
	
    # Compute  base data
    DataSmooth = uniform_filter(aData, size=(0,otherargs.WinSpatSize,otherargs.WinSpatSize))

    Ratio = aData / DataSmooth
    
    ENL = ComputeEntropy(aData, otherargs.WinSpatSize)
    ENLChange = GetStableIdPart(ENL,otherargs.ENLRef,aData.shape).astype(np.int32, copy=False)
    
    # If previous filtering we replace the the first ratio band by the previous one
    if otherargs.PrevFilt:
        Ratio[0,:,:] = inputs.imgs[-2][0,:,:]
    
    NumStableTime = np.bincount(ENLChange.flat)
    SumPart = np.bincount(ENLChange.flat, weights=Ratio.flat)
    
    ArrayNumStableTime = NumStableTime[ENLChange].astype(np.int32, copy=False)
    ArraySumPart = SumPart[ENLChange].astype(np.float32, copy=False)
    
    
    # If previous filt we have to add to the standard numeb of file use for each pixel
    # To normalise the sum part the one from previous filtering
    # We search all the pixel that are stable with the previous one to update its value
    # by adding the old one
    if otherargs.PrevFilt:
        ToAdd = inputs.imgs[-1][0,:,:]
        # Loop thru band to add only id equal to the old  one
        for i in range(ArrayNumStableTime.shape[0]):
            Wh = ENLChange[i,:,:] <= np.prod(ENLChange.shape[1:3])
            ArrayNumStableTime[i,:,:][Wh] += ToAdd[Wh] - 1
        DataFiltered = DataSmooth[1:,:,:] / (ArrayNumStableTime[1:,:,:]) * ArraySumPart[1:,:,:]

    else:
        DataFiltered = DataSmooth / ArrayNumStableTime * ArraySumPart

    DataFiltered = DataFiltered.astype(np.float32, copy=False)
    for i in range(DataFiltered.shape[0]):
        DataFiltered[i,:,:][WhNoise] = 0.001
    #DataFiltered[WhNoise] = 0.001
	
    # Manage output
    # List of numpy array for the output
    OutList = []
    for i in range(DataFiltered.shape[0]):
        OutList.append(np.expand_dims(DataFiltered[i,:,:], axis=0))
    
    # remove border effect
    ArraySumPart = ArraySumPart[-1,:,:]
    ArraySumPart[WhNoise] = 0.001
    # Return to RIOS le sumpart of the last dates to be able to use it next processing time
    OutList.append(np.expand_dims(ArraySumPart, axis=0))
    #OutList.append(np.expand_dims(ArraySumPart[-1,:,:], axis=0))

    # Return to RIOS le number of date use to compute the sum part
    # remove border effect
    ArrayNumStableTime = ArrayNumStableTime[-1,:,:]
    ArrayNumStableTime[WhNoise] = 0
    # of the last dates to be able to use it next processing time
    #OutList.append(np.expand_dims(ArrayNumStableTime[-1,:,:], axis=0))
    OutList.append(np.expand_dims(ArrayNumStableTime, axis=0))

    outputs.imgs = OutList

'''
This function apply teporal filtering over a 3d numpy array
'''
def QueganTemporalSpeckleFilteringRIOSVMean(info, inputs, outputs, otherargs):
    # Test medfilt
    eps = 1e-16

    # To initiate
    aData = inputs.imgs[0]

    # We get data in np array
    for img in inputs.imgs[1:-2]:
        # We remove the 1d band dimension due to RIOS
        aData = np.concatenate((aData ,img), axis=0)

    # Compute change part
    ENL = ComputeENL(aData,otherargs.WinTimeSize, otherargs.WinSpatSize)

    ENLChange = GetStableIdPart(ENL,otherargs.ENLRef,aData.shape).astype(np.int32, copy=False)

    # Compute  base data
    DataSmooth = uniform_filter(aData, size=(0,otherargs.WinSpatSize,otherargs.WinSpatSize))

    Ratio = aData / DataSmooth
    
    # If previous filtering we replace the the first ratio band by the previous one
    if otherargs.PrevFilt:
        Ratio[0,:,:] = inputs.imgs[-2][0,:,:]
    
    NumStableTime = np.bincount(ENLChange.flat)
    SumPart = np.bincount(ENLChange.flat, weights=Ratio.flat)
    
    ArrayNumStableTime = NumStableTime[ENLChange].astype(np.int32, copy=False)
    ArraySumPart = SumPart[ENLChange].astype(np.float32, copy=False)


    
    # If previous filt we have to add to the standard numeb of file use for each pixel
    # To normalise the sum part the one from previous filtering
    # We search all the pixel that are stable with the previous one to update its value
    # by adding the old one
    if otherargs.PrevFilt:
        ToAdd = inputs.imgs[-1][0,:,:]
        # Loop thru band to add only id equal to the old  one
        for i in range(ArrayNumStableTime.shape[0]):
            Wh = ENLChange[i,:,:] <= np.prod(ENLChange.shape[1:3])
            ArrayNumStableTime[i,:,:][Wh] += ToAdd[Wh] - 1
    #     DataFiltered = DataSmooth[1:,:,:] / (ArrayNumStableTime[1:,:,:]) * ArraySumPart[1:,:,:]

    # else:
    #     DataFiltered = DataSmooth / ArrayNumStableTime * ArraySumPart

    
    DataFiltered = np.bincount(ENLChange.flat, weights=aData.flat)[ENLChange] / ArrayNumStableTime
    DataFiltered = DataFiltered.astype(np.float32, copy=False)

    # Manage output
    # List of numpy array for the output
    OutList = []
    for i in range(DataFiltered.shape[0]):
        OutList.append(np.expand_dims(DataFiltered[i,:,:], axis=0))
    
    # Return to RIOS le sumpart of the last dates to be able to use it next processing time
    OutList.append(np.expand_dims(ArraySumPart[-1,:,:], axis=0))

    # Return to RIOS le number of date use to compute the sum part
    # of the last dates to be able to use it next processing time
    OutList.append(np.expand_dims(ArrayNumStableTime[-1,:,:], axis=0))

    outputs.imgs = OutList

def GetNewDatesAndOldOneFromListFiles(aInputList, aInputDir, aOutputDir):
    # List all unique date already processed (output folder)
    AllOutputFiles = GetFileByExtensionFromDirectory(aOutputDir, 'tiff') +  GetFileByExtensionFromDirectory(aOutputDir, 'tif')
    AllOutputFiles = [ file.replace("\\","/") for file in AllOutputFiles if 'TempProcStack' not in file]
    # List all unique date in input file
    AllInputFiles = GetFileByExtensionFromDirectory(aInputDir, 'tiff') +  GetFileByExtensionFromDirectory(aInputDir, 'tif')
    AllInputFiles = [ file.replace("\\","/") for file in AllInputFiles if 'TempProcStack' not in file]

    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOutputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllInputFiles ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]

    UniqueInputDates = list(set(ListDates))
    UniqueInputDates.sort()

    # Get New dates
    NewDates = [x for x in UniqueInputDates if x not in UniqueOutputDates]

    # Add the new one to the list
    # print 'UniqueInputDates, UniqueOutputDates, NewDates', UniqueInputDates, UniqueOutputDates, NewDates

    # Filter the data
    NewList = []
    for NDate in NewDates:
        for item in aInputList:
            if NDate in item:
                NewList.append(item)

    # print 'NewList', NewList
    # We add the already processed
    OldestNewDataIndex = UniqueInputDates.index(NewDates[0])
    OldestNewData = UniqueInputDates[OldestNewDataIndex - 1]

    for item in AllOutputFiles:
        if OldestNewData in item:
            NewList.append(item.replace("\\","/"))
            # print '\n\nitem append, item, NewList', item, NewList
    NewList.sort()

    return NewList

def TileRasterProcessingEWMARIOS(aInputRasterListPath,  aOutputFolder, aBlockSize,\
                                 aAlpha, aRefRadiometry, \
                                 aForestMeanRadiometry, aMedianFiltering, aUpdate, aNumProcCPUToUse):
    
    OutputListFile = []
    for i, FilePath in enumerate(aInputRasterListPath):
        FileDir = os.path.dirname(FilePath)
        # print '\nInput from vrt ', VRTFNameList[index]
        MainFileDir = os.path.split(FileDir)[0]
        FName = os.path.basename(os.path.splitext(FilePath)[0])

        FileName = os.path.join(FileDir,FName + '_EWMA' + str(aAlpha) + '.tif')
        FileName = FileName.replace("\\","/")
        FileName = FileName.replace(MainFileDir + '/',aOutputFolder + '/')
        FileName.replace("\\","/")

        # Create output directory
        if not os.path.exists(os.path.dirname(FileName)):
            os.makedirs(os.path.dirname(FileName))
        if aUpdate  == True:
            # print '\n Ouput in update ', FileName
            if i > 0:
                OutputListFile.append(FileName)
        else:
            OutputListFile.append(FileName)
    
    
    
    
    # Settings of RIOS lib
    # Beacause we need overlap between blocks
    controls = applier.ApplierControls()

    # Define out format
    controls.setOutputDriverName("GTiff")
    #~ controls.setCreationOptions(["COMPRESS=DEFLATE",  'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES'])
    controls.setCreationOptions(["COMPRESS=DEFLATE", 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES', 'NBITS=16'])
    
    # Enable multi thread
    controls.setNumThreads(aNumProcCPUToUse)
    controls.setJobManagerType('multiprocessing')
    
    # Give the block size
    
    aBlockSize = np.power(2,int(np.log2(aBlockSize)))
    if aBlockSize > 256:
        aBlockSize = 256

    controls.setWindowXsize(aBlockSize)
    controls.setWindowYsize(aBlockSize)
    
    # Enable progress
#    controls.progress = cuiprogress.GDALProgressBar()
    
    # Define other args to use
    otherargs = applier.OtherInputs()

    otherargs.alpha = aAlpha
    otherargs.Update= aUpdate
    
    otherargs.RefRad = aRefRadiometry
    otherargs.ForestMeanRadiometry = aForestMeanRadiometry
    
    otherargs.MedianFiltering = aMedianFiltering
    
    # Because we use an arbitrary number of input output
    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    
    infiles.imgs = aInputRasterListPath
    outfiles.imgs = OutputListFile


    # Allow ressampling in case of little bit different footprint due to tile for example
    controls.setReferenceImage(infiles.imgs[0])
    
    applier.apply(EWMARios, infiles, outfiles,otherargs, controls=controls)

def MedianTimeFiltering(aData,aWinSize):
    aDataMed = medfilt(aData, (aWinSize, 1,1))

    return aDataMed

def EWMARios(info, inputs, outputs, otherargs):
    aRefRadiometry = otherargs.RefRad
    aAlpha = otherargs.alpha
    aForestMeanRadiometry = otherargs.ForestMeanRadiometry
    MedFilt = otherargs.MedianFiltering
    # To initiate
    aData = np.zeros(shape=(len(inputs.imgs), (inputs.imgs[0]).shape[1], (inputs.imgs[0]).shape[2]),dtype=np.float32)
    
    

    # We get data in np array
    for i in range(len(inputs.imgs)):
        aData[i,:,:] = inputs.imgs[i]
    
    # Get Nodata pixel
    WhNoise = aData[0,:,:] == -30.
    # Calculate exponential to enhance dynamics
    if MedFilt == 'True':
        ExpRefSmooth = np.array(aRefRadiometry)[:, None, None] - aData
        ExpRefSmooth = MedianTimeFiltering(np.exp(ExpRefSmooth),3).astype(np.float32, copy=False)
        # ExpRefSmooth = MedianTimeFiltering(np.array(aRefRadiometry)[:, None, None] - Data,3)
    else:
        ExpRefSmooth = np.array(aRefRadiometry)[:, None, None] - aData
        ExpRefSmooth = np.exp(ExpRefSmooth).astype(np.float32, copy=False)
        
    if otherargs.Update  == True:
        ExpRefSmooth[0,:,:] = np.exp(np.array(aForestMeanRadiometry)[np.newaxis,np.newaxis,np.newaxis] - aData[0,:,:]  )

            # ExpRefSmooth = np.exp(ExpRefSmooth)

            # Apply EWMA
    ExpRefSmooth = EWMA(ExpRefSmooth, aAlpha)
    
    if aForestMeanRadiometry == 0.:
        ExpRefSmooth = np.mean(aRefRadiometry)[np.newaxis,np.newaxis,np.newaxis] - np.log(ExpRefSmooth)
    else:
        ExpRefSmooth =  np.array(aForestMeanRadiometry)[np.newaxis,np.newaxis,np.newaxis] - np.log(ExpRefSmooth)
    
    # Mask no data areas
    for i in range(ExpRefSmooth.shape[0]):
        ExpRefSmooth[i,:,:][WhNoise] = -30
    
    OutList = []
    if otherargs.Update  == True:
        for i in range(ExpRefSmooth.shape[0]-1):
            OutList.append(np.expand_dims(ExpRefSmooth[i+1,:,:].astype(np.float32, copy=False), axis=0))
            # OutList.append(np.expand_dims(ExpRefSmooth[i,:,:], axis=0))
            #print '\n\n update out i+1 ExpRefSmooth.shape[0]', i+1, ExpRefSmooth.shape[0]
    else:
        for i in range(ExpRefSmooth.shape[0]):
            OutList.append(np.expand_dims(ExpRefSmooth[i,:,:].astype(np.float32, copy=False), axis=0))
            # OutList.append(np.expand_dims(ExpRefSmooth[i,:,:], axis=0))

    outputs.imgs = OutList


def EWMA(aData, aAlpha):
    Smooth = np.zeros(shape=aData.shape,dtype=np.float32)
    SmoothOld = np.zeros(shape=aData.shape[1:3],dtype=np.float32)

    SmoothOld = aData[0,:,:]
    Smooth[0,:,:] = aData[0,:,:]

    for i in range(1,aData.shape[0]):
        Smooth[i,:,:] = aAlpha * aData[i,:,:] + (1- aAlpha) * SmoothOld
        SmoothOld = Smooth[i,:,:]

    return Smooth.astype(np.float32)
    
'''
Apply block processing temporal filtering
'''
def TileTemporalFilteringRIOSV2(aInputRasterListPath, aOutputRasterListPath,
                            aInputPrevFiltSumPartPath, aInputPrevFiltCountPath, aBlockSize,aTempWindowSize, aENLRef, aOutput_Folder, aNumProcCPUToUse):
    # Boolean of previous filtering
    BoolPrevFilt = False
    aListDates =[]

    aListDates = [getDayFromS1FileOrFolder(aRastPath) for aRastPath in aInputRasterListPath ]
    # Remove None element if there is
    aListDates = [i for i in aListDates if i]

    UniqueDates = list(set(aListDates))
    UniqueDates.sort()
    
    # Test if it's first processing time or if previous filtering files is existing
    if 'TempProcStack' in aInputPrevFiltSumPartPath:
        # Process Previous SumPart file
        # Get first and last dates and number of date from this format QueganVV_20150221_20161118_15.tif
        BaseNameSumPart = os.path.basename(os.path.splitext(aInputPrevFiltSumPartPath)[0])

        InputPrevFiltNameSplitSumPart = BaseNameSumPart.split('_')

        # Update the last date for output one
        InputPrevFiltNameSplitSumPart[2] = UniqueDates[-1]

        aOuputPrevFiltPathSumPart = modOsJoinPath([os.path.dirname(aInputPrevFiltSumPartPath) ,
                                          '_'.join(InputPrevFiltNameSplitSumPart) + '.tif'])

        # Process Previous Count file
        BaseNameCount = os.path.basename(os.path.splitext(aInputPrevFiltCountPath)[0])

        InputPrevFiltNameSplitCount = BaseNameCount.split('_')

        # Update the last date for output one
        InputPrevFiltNameSplitCount[2] = UniqueDates[-1]

        aOuputPrevFiltPathCount = modOsJoinPath([os.path.dirname(aInputPrevFiltCountPath) ,
                                          '_'.join(InputPrevFiltNameSplitCount) + '.tif'])
        BoolPrevFilt = True
    else:
        # Even if it does not exist we have to Put existing file in order that rios find it in input
        aInputPrevFiltSumPartPath = aInputRasterListPath[0]
        aInputPrevFiltCountPath = aInputRasterListPath[0]
    
        if '_VV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPathSumPart = modOsJoinPath([aOutput_Folder,
            'TempProcStackSumPartVV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

            aOuputPrevFiltPathCount = modOsJoinPath([aOutput_Folder,
            'TempProcStackCountPartVV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

        elif '_HH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPathSumPart = modOsJoinPath([aOutput_Folder,
            'TempProcStackSumPartHH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

            aOuputPrevFiltPathCount = modOsJoinPath([aOutput_Folder,
            'TempProcStackCountPartHH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

        elif '_VH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPathSumPart = modOsJoinPath([aOutput_Folder,
            'TempProcStackSumPartVH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

            aOuputPrevFiltPathCount = modOsJoinPath([aOutput_Folder,
            'TempProcStackCountPartVH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

        elif '_HV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPathSumPart = modOsJoinPath([aOutput_Folder,
            'TempProcStackSumPartHV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])

            aOuputPrevFiltPathCount = modOsJoinPath([aOutput_Folder,
            'TempProcStackCountPartHV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '.tif'])
            
        BoolPrevFilt = False

    
    # Append the Input Prev field path to put in the quegan filtering
    aInputRasterListPath.append(aInputPrevFiltSumPartPath)
    aInputRasterListPath.append(aInputPrevFiltCountPath)
    
    # Append the Output Prev field path to put in the quegan filtering
    aOutputRasterListPath.append(aOuputPrevFiltPathSumPart)
    aOutputRasterListPath.append(aOuputPrevFiltPathCount)
                                
    # Settings of RIOS lib
    # Beacause we need overlap between blocks
    controls = applier.ApplierControls()

    # Define out format
    # controls.setOutputDriverName("GTiff")
    # controls.setCreationOptions(["COMPRESS=DEFLATE",  'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES'])
    # for a 3x3 the overlap is 1, 5x5 overlap is 2 etc
    Radius = int(aTempWindowSize / 2.)
    controls.setOverlap(Radius)
    
    # Enable multi thread
    controls.setNumThreads(aNumProcCPUToUse)
    controls.setJobManagerType('multiprocessing')
    
    # Give the block size
    
    # aBlockSize = np.power(2,int(np.log2(aBlockSize)))

    # Give the block size
    
    aBlockSize = np.power(2,int(np.log2(aBlockSize)))
    if aBlockSize > 256:
        aBlockSize = 256
    
    # Enable progress
#    controls.progress = cuiprogress.GDALProgressBar()
    
    # Define other args to use
    otherargs = applier.OtherInputs()

    # Temporal window size for ENL computing
    otherargs.WinTimeSize = 2

    # Spatial window size for ENL computing and temporal filtering
    # Have to test with Windows equal to 5
    otherargs.WinSpatSize = aTempWindowSize

    otherargs.ENLRef = aENLRef
    
    # If previous filtering exist
    otherargs.PrevFilt = BoolPrevFilt
    
    # Because we use an arbitrary number of input output
    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    
    infiles.imgs = aInputRasterListPath
    outfiles.imgs = aOutputRasterListPath

    # Allow ressampling in case of little bit different footprint due to tile for example
    controls.setReferenceImage(infiles.imgs[0])
    
    applier.apply(QueganTemporalSpeckleFilteringRIOSV2, infiles, outfiles,otherargs, controls=controls)
    

def QueganLeeTemporalSpeckleFilteringTest(aData, aSumPart, aENLChange, aPrevNumDates, aWinSize, aPrevFilt):
    '''
    SI prev filt
        alors le ratio de t-1 egal à aSumpart
        Une fois Arraynumstable calculer, extraire tous les id de Numstable time présent à t-1
        et y ajouter le aPrevNumdates
    '''
    # Compute  base data
    DataSmooth = uniform_filter(aData, size=(0,aWinSize,aWinSize))
    Ratio = aData / DataSmooth
    
    # If previous filtering we replace the the first ratio band by the previous one
    if aPrevFilt:
        Ratio[0,:,:] = aSumPart
    
    NumStableTime = np.bincount(aENLChange.flat)
    SumPart = np.bincount(aENLChange.flat, weights=Ratio.flat)
    
    ArrayNumStableTime = NumStableTime[aENLChange]
    ArraySumPart = SumPart[aENLChange]
    
    # If previous filt we have to add to the standard numeb of file use for each pixel
    # To normalise the sum part the one from previous filtering
    # We search all the pixel that are stable with the previous one to update its value
    # by adding the old one
    if aPrevFilt:
        ToAdd = ArrayNumStableTime[0,:,:]
        # Loop thru band to add only id equal to the old  one
        for i in range(zone.shape[0]):
            Wh = aENLChange[i,:,:] <= np.prod(aENLChange.shape[1:3])
            ArrayNumStableTime[i,:,:][Wh] += ToAdd[Wh]
    
    DataFiltered = DataSmooth / ArrayNumStableTime * ArraySumPart
    
    return DataFiltered, ArraySumPart[-1,:,:], ArrayNumStableTime[-1,:,:]
        
'''
Apply block processing temporal filtering
'''
def TileTemporalFilteringRIOS(aInput_Data_Folder, aInputRasterListPath, aOutputRasterListPath,
                            aInputPrevFiltPath, aBlockSize,aTempWindowSize, aOutput_Folder):
    NumBands = len(aInputRasterListPath)
    # Boolean of previous filtering
    BoolPrevFilt = False

    # We get InputRaster dates
    aOuputPrevFiltPath = ''
    NumPrevDates = 0
    aListDates = [getDayFromS1FileOrFolder(aRastPath) for aRastPath in aInputRasterListPath ]
    # Remove None element if there is
    aListDates = [i for i in aListDates if i]


    UniqueDates = list(set(aListDates))
    UniqueDates.sort()
    
    # Test if it's first processing time or if previous filtering files is existing
    if aInputPrevFiltPath != '':
        # Get first and last dates and number of date from this format QueganVV_20150221_20161118_15.tif
        BaseName = os.path.basename(os.path.splitext(aInputPrevFiltPath)[0])
        NumPrevDates = float(BaseName.split('_')[3])

        InputPrevFiltNameSplit = BaseName.split('_')
        InputPrevFiltNameSplit[2] = UniqueDates[-1]
        InputPrevFiltNameSplit[3] = str(int(NumPrevDates + NumBands))

        aOuputPrevFiltPath = modOsJoinPath([os.path.dirname(aInputPrevFiltPath) ,
                                          '_'.join(InputPrevFiltNameSplit) + '.tif'])
        BoolPrevFilt = True
    else:
        # Put existing file in order that rios find it in input
        aInputPrevFiltPath = aInputRasterListPath[0]
    
        if '_VV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder,
            'TempProcStackVV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_HH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder,
            'TempProcStackHH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_VH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder,
            'TempProcStackVH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_HV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder,
            'TempProcStackHV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
            
        BoolPrevFilt = False

    # Append the Input Prev field path to put in the quegan filtering
    aInputRasterListPath.append(aInputPrevFiltPath)
    
    # Append the Output Prev field path to put in the quegan filtering
    aOutputRasterListPath.append(aOuputPrevFiltPath)
                                
    # Settings of RIOS lib
    # Beacause we need overlap between blocks
    controls = applier.ApplierControls()

    # controls.setOutputDriverName("GTiff")
    # controls.setCreationOptions(["COMPRESS=DEFLATE",  'PREDICTOR=3', 'ZLEVEL=9', 'BIGTIFF=YES' , 'TILED=YES'])
    # for a 3x3 the overlap is 1, 5x5 overlap is 2 etc
    Radius = int(aTempWindowSize / 2.)
    controls.setOverlap(Radius)
    
    # Enable multi thread
    controls.setNumThreads(1)
    controls.setJobManagerType('multiprocessing')
    
    # Give the block size
    # aBlockSize = np.power(2,int(np.log2(aBlockSize)))
    controls.setWindowXsize(aBlockSize)
    controls.setWindowYsize(aBlockSize)
    
    # Enable progress
#    controls.progress = cuiprogress.GDALProgressBar()
    
    # Define other args to use
    otherargs = applier.OtherInputs()
    # Window size
    otherargs.WinSize = aTempWindowSize
    
    # If previous filtering exist
    otherargs.PrevFilt = BoolPrevFilt
    otherargs.NPrevDates = NumPrevDates
    
    # Because we use an arbitrary number of input output
    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    
    infiles.imgs = aInputRasterListPath
    outfiles.imgs = aOutputRasterListPath

    # Allow ressampling in case of little bit different footprint due to tile for example
    controls.setReferenceImage(infiles.imgs[0])
    
    applier.apply(QueganTemporalSpeckleFilteringRIOS, infiles, outfiles,otherargs, controls=controls)

    
'''
Apply block processing temporal filtering
'''
def TileTemporalFiltering(aInput_Data_Folder, aInputRasterListPath, aOutputRasterListPath,
                          aInputPrevFiltPath, aBlockSize,aTempWindowSize, aOutput_Folder):
    '''
    TODO: Add sliding window parameter


    This method show how to process huge raster
    using easy tilling based on block size
    and if necessary taking into account window size
    when necessary in processing like filtering for example
    aInputRasterPath string --> Contain input raster path to process
    aOutputRasterPath string --> Contain output raster path to process
    aBlockSize integer --> Contain the size of square block to Tile
    aWindowSize integer --> Contain windows size for sliding window (0 size by default)
    '''
    aWindowSize = aTempWindowSize

    # we put X and Y block size based on aBlockSize
    xBSize = aBlockSize
    yBSize = aBlockSize


    NumBands = len(aInputRasterListPath)

    # We get InputRaster dates
    aOuputPrevFiltPath = ''
    NumPrevDates = 0
    aListDates = [getDayFromS1FileOrFolder(aRastPath) for aRastPath in aInputRasterListPath ]
    # Remove None element if there is
    aListDates = [i for i in aListDates if i]

    UniqueDates = list(set(aListDates))
    UniqueDates.sort()



    # We open one raster to get rows and cols
    src_ds = gdal.Open( aInputRasterListPath[0] )
    if src_ds is None:
        print('Could not open ' + fn)
        sys.exit(1)

    # We get number of row and cols
    rows = src_ds.RasterYSize
    cols = src_ds.RasterXSize

    # We force Float32 to read data
    BandType = gdal.GDT_Float32

    # We get Projection from input raster
    InputGeoTransform = src_ds.GetGeoTransform()
    InputProjection = src_ds.GetProjection()

    src_ds = None

    # Open input files
    GdalFilePointerList = []
    GdalOutputFilePointerList = []
    InputBandPointerlist = []
    OutputBandPointerlist = []

    for i in range(NumBands):
        GdalFilePointerList.append(gdal.Open( aInputRasterListPath[i] ))
        InputBandPointerlist.append( GdalFilePointerList[i].GetRasterBand(1) )
    
    if aInputPrevFiltPath != '':
        src_Sumpart = gdal.Open( aInputPrevFiltPath )
        SumpartBand = src_Sumpart.GetRasterBand(1)
        if src_Sumpart is None:
            print('Could not open ' + fn)
            sys.exit(1)
        # Get first and last dates and number of date from this format QueganVV_20150221_20161118_15.tif
        BaseName = os.path.basename(os.path.splitext(aInputPrevFiltPath)[0])
        NumPrevDates = float(BaseName.split('_')[3])

        # print 'BaseName, FiltBeginDate, FiltEnDdate, NumPrevDates', BaseName, FiltBeginDate, FiltEnDdate, NumPrevDates
        # print UniqueDates[-1], 

        InputPrevFiltNameSplit = BaseName.split('_')
        InputPrevFiltNameSplit[2] = UniqueDates[-1]
        InputPrevFiltNameSplit[3] = str(int(NumPrevDates + NumBands))

        aOuputPrevFiltPath = modOsJoinPath([os.path.dirname(aInputPrevFiltPath) , '_'.join(InputPrevFiltNameSplit) + '.tif'])
    else:
        # print 'Prev path does not  exist'
        if '_VV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder, 'TempProcStackVV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_HH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder, 'TempProcStackHH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_VH_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder, 'TempProcStackVH_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])
        elif '_HV_' in aInputRasterListPath[0]:
            aOuputPrevFiltPath = modOsJoinPath([aOutput_Folder, 'TempProcStackHV_' + UniqueDates[0] + '_' + UniqueDates[-1] + '_' + str(NumBands) + '.tif'])

    # Output file
    # print 'aOuputPrevFiltPath', aOuputPrevFiltPath
    format = "GTiff"
    driver = gdal.GetDriverByName( format )
    for i in range(NumBands):
        GdalOutputFilePointerList.append(driver.Create(aOutputRasterListPath[i], cols, rows, 1, BandType ))
        GdalOutputFilePointerList[i].SetGeoTransform(InputGeoTransform)
        GdalOutputFilePointerList[i].SetProjection(InputProjection)

        OutputBandPointerlist.append(GdalOutputFilePointerList[i].GetRasterBand(1))
        OutputBandPointerlist[i].SetNoDataValue(0)

    # Add SUmpart Output
    GdalOutputFilePointerList.append(driver.Create(aOuputPrevFiltPath, cols, rows, 1, BandType ))
    GdalOutputFilePointerList[NumBands].SetGeoTransform(InputGeoTransform)
    GdalOutputFilePointerList[NumBands].SetProjection(InputProjection)
    
    OutputBandPointerlist.append(GdalOutputFilePointerList[NumBands].GetRasterBand(1))
    OutputBandPointerlist[NumBands].SetNoDataValue(0)

    # print 'Rows, Cols: ',rows, cols
    BlockId = 0
    for i in range(0, rows, yBSize):
        if i + yBSize < rows:
            numRows = yBSize
        else:
            numRows = rows - i
        # Backup i and numRows
        numRowsDefault = numRows
        iOriginal = i
        for j in range(0, cols, xBSize):
            i = iOriginal
            numRows = numRowsDefault
            # print '\n Block numero : ', BlockId
            if j + xBSize < cols:
                numCols = xBSize
            else:
                numCols = cols - j
            numColsDefault = numCols

            # print 'Indice i,j original : ', i, j
            # print 'numRows numCols :',numRows, numCols
            # Test for applying sliding window buffer
            # Backup j
            jOriginal = j

            iOffset = 0
            jOffset = 0
            numColsOffset = 0
            numRowsOffset = 0
            if i - aWindowSize >= 0:
                i = i - aWindowSize
                iOffset = aWindowSize
            if j - aWindowSize >= 0:
                j = j - aWindowSize
                jOffset = aWindowSize

            if jOriginal + numCols + aWindowSize <= cols:
                numCols = numCols + aWindowSize
                numColsOffset = aWindowSize
            numCols = numCols + jOffset

            if iOriginal + numRows + aWindowSize <= rows:
                numRows = numRows + aWindowSize
                numRowsOffset = aWindowSize
            numRows = numRows + iOffset

            # print 'Read as array j, i, numCols, numRows', j, i, numCols, numRows
            Data = np.zeros(shape=(NumBands, numRows, numCols ),dtype=float)
            SumPart = np.zeros(shape=(numRows, numCols ),dtype=float)

            #We get file values
            for it in range(len(InputBandPointerlist)):
                Data[it, :,:] = InputBandPointerlist[it].ReadAsArray(j, i, numCols, numRows)

            # Test if Previous filtering
            if aInputPrevFiltPath != '':
                # print 'existing filt file'
                
                SumPart = SumpartBand.ReadAsArray(j, i, numCols, numRows)

            '''
            Do something like
            '''
            # Temporal filtering
            Data, SumPart = QueganTemporalSpeckleFiltering(Data,SumPart, NumPrevDates, aTempWindowSize)

            #Clip the border
            Data = Data[:,iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]
            SumPart = SumPart[iOffset:iOffset + numRowsDefault,jOffset:jOffset + numColsDefault]


            #We writte Quegan filtered data
            for band in range( NumBands):
                OutputBandPointerlist[band].WriteArray(Data[band,:,:],jOriginal,iOriginal)
            
            OutputBandPointerlist[NumBands].WriteArray(SumPart,jOriginal,iOriginal)

            BlockId = BlockId + 1

    # We close all file
    src_ds = None
    for band in range( NumBands + 1):
        GdalOutputFilePointerList[band] = None
        OutputBandPointerlist[band] = None
    for band in range( NumBands):
        GdalFilePointerList[band] = None
        InputBandPointerlist[band] = None

def GenerateDualPolColorcompositiondBSingle(aCopolFile, aCrosspolFile, aRam):
    CopoldBFile = aCopolFile.replace('VV_', 'VVdB_')
    CrosspoldBFile = aCopolFile.replace('VV_', 'VHdB_')

    DiffFile = aCopolFile.replace('VV_', 'VHdB-VVdB_')

    OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VVdB_VHdB_VHdB-VVdB_')
    OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

    Int2dB(aCopolFile, 'im1b1', CopoldBFile, aRam)
    Int2dB(aCrosspolFile, 'im1b1', CrosspoldBFile, aRam)
    DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, aRam)


    # Del IntCopol
    if os.path.exists(aCopolFile):
        os.remove(aCopolFile)

	# Del IntCrosspol
    if os.path.exists(aCrosspolFile):
        os.remove(aCrosspolFile)

	# VRT color composition
    GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  0, 0)
	
    return OutputColorCompVRTFile

def GenerateDualPolColorcompositiondBFromdB(aSubfolders, aOutputFolder, aRam):
    # Loop thru different S1 data (folder)
    for Folder in aSubfolders:
        # List all tif and tiff files
        AllTifFile = GetFileByExtensionFromDirectory(Folder, 'tif')

        InDirName = os.path.split(Folder)[1]

        # Create Output subfolder
        OutFolder = os.path.join(aOutputFolder,InDirName)
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)

        Dual = False
        if '1SDV' in Folder:
            CopolFile = [s for s in AllTifFile if "VV" in s][0]
            CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
            Dual = True

        elif '1SDH' in Folder:
            CopolFile = [s for s in AllTifFile if "HH" in s][0]
            CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
            Dual = True
        else:
            print('Not dual pol data')

        if Dual:
            CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])

            if '1SDV' in Folder:
                BaseNameFile = CopolFileName.replace('VV','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = os.path.join(OutFolder,  BaseNameFile + '_VHdB-VVdB' +'.tif')

                OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'VVdB_VHdB_HVdB-VVdB' +'.vrt')
            else:
                BaseNameFile = CopolFileName.replace('HH','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = os.path.join(OutFolder,  BaseNameFile + 'HVdB-HHdB' +'.tif')

                OutputColorCompVRTFile = os.path.join(OutFolder,  BaseNameFile + 'HHdB_HVdB_HVdB-HHdB' +'.vrt')

            # test if file is already existing
            if not os.path.exists(DiffFile):
                DiffdB(CopolFile, CrosspolFile, DiffFile, aRam)

            # VRT color composition
            if not os.path.exists(OutputColorCompVRTFile):
                GdalBuildVRT([CopolFile, CrosspolFile, DiffFile], OutputColorCompVRTFile,  -30, -30)
'''
This function generate dual pol color composition in dB
'''
def GenerateDualPolColorcompositiondB(aSubfolders, aOutputFolder, aRam):
    # Loop thru different S1 data (folder)
    for Folder in aSubfolders:
        # List all tif and tiff files
        AllTifFile = GetFileByExtensionFromDirectory(Folder, 'tif')

        InDirName = os.path.split(Folder)[1]

        # Create Output subfolder
        OutFolder = modOsJoinPath([aOutputFolder,InDirName])
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)

        Dual = False
        if '1SDV' in Folder:
            CopolFile = [s for s in AllTifFile if "VV" in s][0]
            CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
            Dual = True

        elif '1SDH' in Folder:
            CopolFile = [s for s in AllTifFile if "HH" in s][0]
            CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
            Dual = True
        else:
            print('Not dual pol data')

        if Dual:
            CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
            CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

            CopoldBFile = modOsJoinPath([OutFolder,  CopolFileName + '_dB' +'.tif'])
            CrosspoldBFile = modOsJoinPath([OutFolder,  CrosspolFileName + '_dB' +'.tif'])

            if '1SDV' in Folder:
                BaseNameFile = CopolFileName.replace('VV','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = modOsJoinPath([OutFolder,  BaseNameFile + '_VHdB-VVdB' +'.tif'])

                OutputColorCompVRTFile = modOsJoinPath([OutFolder,  BaseNameFile + 'VVdB_VHdB_HVdB-VVdB' +'.vrt'])
            else:
                BaseNameFile = CopolFileName.replace('HH','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                DiffFile = modOsJoinPath([OutFolder,  BaseNameFile + 'HVdB-HHdB' +'.tif'])

                OutputColorCompVRTFile = modOsJoinPath([OutFolder,  BaseNameFile + 'HHdB_HVdB_HVdB-HHdB' +'.vrt'])
            
            Int2dB(CopolFile, 'im1b1', CopoldBFile, aRam)
            Int2dB(CrosspolFile, 'im1b1', CrosspoldBFile, aRam)
            DiffdB(CopoldBFile, CrosspoldBFile, DiffFile, aRam)



            # VRT color composition
            GdalBuildVRT([CopoldBFile, CrosspoldBFile, DiffFile], OutputColorCompVRTFile,  -30, -30)

'''
This function create difference between copol and crosspol
'''
def DiffdB(aInputFileCopol, aInputFileCrosspol, aOutputFile, aRam):
    # "out.tif?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES" 
    cmd = "otbcli_BandMath -il "
    cmd += aInputFileCopol + " "
    cmd += aInputFileCrosspol + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputFile + "?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES\""
    cmd += " -exp "
    cmd += "\" im2b1 - im1b1 \""

    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

'''
This function generate a vrt from input raster list
'''
def GdalBuildVRT(aInputListFile, aOutputFile,  aSRCNoData, aVRTNoData):
    TmpListfile = aOutputFile.replace('.vrt', 'TMPListFile.txt')
    fic=open(TmpListfile,'w')
    for file in aInputListFile:
        fic.write(file +'\n')
    fic.close()

    cmd = "gdalbuildvrt "
    cmd += " -separate -overwrite "
    cmd += " -srcnodata " + str(aSRCNoData)
    cmd += " -vrtnodata " + str(aVRTNoData)
    cmd += " " + aOutputFile
    cmd += " -input_file_list " + TmpListfile

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

    if os.path.exists(TmpListfile):
        os.remove(TmpListfile)

'''
This function return all pixel values overlayed by a polygon
For each pixel it return the value of requested band and
The label in the given Vector field
aInputVector -> path of the Input Polygon
aFielName    -> field name of the vector file that contain the label
aInputRaster -> path of the raster to get the pixel band values
aListBands   -> list of the bands number to get the pixels

return
first, one list of y,x,ygeo,xgeo that contain pixel coordinate and geographical
second, one list where first sublist is label, next sublist contain the pixel value
'''
def GetRasterBandsPixelsValuesFromVectorFile(aInputVector, aFielName, aInputRaster, aListBands, aMinSamplingPointPerFeature):
    aFielName = str(aFielName)
    # List that will store respectivaly Label, feature id and band pixel values
    StoreList = [[] for x in range(len(aListBands)+2)]

    # lIst that contain first pixel row cols next in geographical
    PixelCoordList = [[] for x in range(4)]

    # Open Raster data
    raster = gdal.Open(aInputRaster)

    # Get raster georeference info
    transform = raster.GetGeoTransform()
    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]

    # Open Vector data
    shp = ogr.Open(aInputVector)
    lyr = shp.GetLayer()

    # Reproject vector geometry to same projection as raster
    sourceSR = lyr.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(raster.GetProjectionRef())
    coordTrans = osr.CoordinateTransformation(sourceSR,targetSR)

    # Get number of Vector feature
    NumFeature = lyr.GetFeatureCount()


    for iFeat in range(NumFeature):
        # Get the input Feature
        feat = lyr.GetFeature(iFeat)

        geom = feat.GetGeometryRef()
        geom.Transform(coordTrans)

        # Get extent of feat
        geom = feat.GetGeometryRef()

        # Get Field Value
        FValue = feat.GetField(aFielName)
        if (geom.GetGeometryName() == 'MULTIPOLYGON'):
            count = 0
            pointsX = []; pointsY = []
            for polygon in geom:
                geomInner = geom.GetGeometryRef(count)
                ring = geomInner.GetGeometryRef(0)
                numpoints = ring.GetPointCount()
                for p in range(numpoints):
                    lon, lat, z = ring.GetPoint(p)
                    pointsX.append(lon)
                    pointsY.append(lat)
                count += 1
        elif (geom.GetGeometryName() == 'POLYGON'):
            ring = geom.GetGeometryRef(0)
            numpoints = ring.GetPointCount()
            pointsX = []; pointsY = []
            for p in range(numpoints):
                lon, lat, z = ring.GetPoint(p)
                pointsX.append(lon)
                pointsY.append(lat)

        else:
            sys.exit("ERROR: Geometry needs to be either Polygon or Multipolygon")

        xmin = min(pointsX)
        xmax = max(pointsX)
        ymin = min(pointsY)
        ymax = max(pointsY)

        # Specify offset and rows and columns to read
        xoff = int((xmin - xOrigin)/pixelWidth)
        #yoff = int((yOrigin - ymax)/(-pixelHeight))
        yoff = int((yOrigin - ymax)/pixelWidth)
        xcount = int((xmax - xmin)/pixelWidth)+1
        ycount = int((ymax - ymin)/(-pixelHeight))+1
        #ycount = int((ymax - ymin)/pixelWidth)+1

        # Create memory target raster to store Polygon mask area
        target_ds = gdal.GetDriverByName('MEM').Create('', xcount, ycount, 1, gdal.GDT_Byte)
        target_ds.SetGeoTransform((
            xmin, pixelWidth, 0,
            ymax, 0, pixelHeight,
        ))

        # Create for target raster the same projection as for the value raster
        raster_srs = osr.SpatialReference()
        raster_srs.ImportFromWkt(raster.GetProjectionRef())
        target_ds.SetProjection(raster_srs.ExportToWkt())

        # Rasterize zone polygon to raster
        gdal.RasterizeLayer(target_ds, [1], lyr, burn_values=[1])

        # Get raster mask in a numpy array
        bandmask = target_ds.GetRasterBand(1)
        datamask = bandmask.ReadAsArray(0, 0, xcount, ycount).astype(float)

        # clear target_ds
        target_ds = None

        # Get label of the current feature
        label = FValue

        # Get pixel coordinate of overlay with polygon
        # rows, cols = np.where(np.logical_not(datamask)) # commande prenant le complementaire du polygone
        rows, cols = np.where(datamask == 1)

        if aMinSamplingPointPerFeature > 0:
            if len(rows) > aMinSamplingPointPerFeature:
                NumPixRange = list(range(len(rows)))
                shuffle(NumPixRange)
                rows = rows[NumPixRange[:aMinSamplingPointPerFeature]]
                cols = cols[NumPixRange[:aMinSamplingPointPerFeature]]



        PixelCoordList[0].extend((rows+yoff).tolist())
        PixelCoordList[1].extend((cols+xoff).tolist())

        # Convert Pixel Coordinate to geographic coordinate
        PixelCoordList[2].extend([ y * pixelHeight + yOrigin  for y in rows+yoff])
        PixelCoordList[3].extend([ x * pixelWidth +  xOrigin  for x in cols+xoff])

        numPixel = len(rows.tolist())

        # Read raster bands as arrays
        for j in range(len(aListBands)):
            bandRaster = raster.GetRasterBand(aListBands[j])
            dataraster = bandRaster.ReadAsArray(xoff, yoff, xcount, ycount).astype(float)

            # Get pixel value of curent band
            bandVal = [dataraster[y,x] for y,x in zip(rows, cols)]

            # Store in StoreList
            StoreList[j+2].extend(bandVal)

        # We store corresponding label in first cols of the list
        StoreList[0].extend([label] * numPixel)
        # We store corresponding feature id in second cols of the list
        StoreList[1].extend([iFeat] * numPixel)



    shp = None
    raster = None
    return PixelCoordList, StoreList

def GetRefTimeRadiometry(aInputRaster, aInputVector, aInputFieldName, aFieldValue):
    # We extract all point in ref polygon so 0
    NumSamplingPointPerFeature = 10000
    # We get vrt file list
    VRTFNameList = GetFilesFromVrt(aInputRaster)
    # Get acquisition time of each data
    FilesTimeStamp = []
    for file in VRTFNameList:
        FileName = os.path.basename(os.path.splitext(file)[0])
        # Time = GetAcquistionTimeFromSentinel1FileName(FileName)
        Time = getDayFromS1FileOrFolder(FileName)


        Time = Time[0:4] + '-' + Time[4:6] + '-' + Time[6:8]

        TimeStamp = pd.Timestamp(Time)
        FilesTimeStamp.append(int(TimeStamp.value /10E9))

    ListBands = [band + 1 for band in range(len(VRTFNameList))]

    # The list of all pixel
    __, PixelLists = GetRasterBandsPixelsValuesFromVectorFile(aInputVector, aInputFieldName, aInputRaster, ListBands,NumSamplingPointPerFeature)

    NumSamplePixel = len(PixelLists[0])

    # Define the col names
    DFColsName = ['label', 'time','sigma0dB']
    df = pd.DataFrame(columns=DFColsName)

    # list to add to dataframe
    LabelList = []
    TimeList = []
    SigmaList = []

    # Add values to the dataframe
    for Bi in range(len(VRTFNameList)):
        LabelList.extend(PixelLists[0])
        TimeList.extend([FilesTimeStamp[Bi]] * NumSamplePixel)
        SigmaList.extend(PixelLists[Bi + 2])

    df['label'] = LabelList
    df['time'] = TimeList
    df['sigma0dB'] = SigmaList

    # Get Stable forest average radiometry for each time
    dfForest = df[df['label'] == aFieldValue]

    # Subset the column (remove label and feature column)
    dfForest = dfForest[["time", "sigma0dB"]]

    # Average all pixel by time
    dfForestAverage = dfForest.groupby(["time"]).mean().unstack("time")

    # Get time list from groupby
    TimeStampFromDf = list(dfForest.groupby(["time"]).groups.keys())

    # Convert to list
    listForest = dfForestAverage.values.T.tolist()

    return sorted(TimeStampFromDf), listForest

def GenerateTemporalAverage(aInputFolder, aOutput_in_dB, aRam):
    AllTifFile = GetFileByExtensionFromDirectory(aInputFolder, 'tif')
    AllTifFile = [ file for file in AllTifFile if 'Quegan' not in file]
    AllCopolFile = [file for file in AllTifFile if ('VV' in file) or  ('HH' in file)]
    AllCrosspolFile = [file for file in AllTifFile if ('HV' in file) or  ('VH' in file)]


	# We get acquisition date
    ListDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllCopolFile ]
    # Remove None element if there is
    ListDates = [i for i in ListDates if i]


    UniqueOutputDates = list(set(ListDates))
    UniqueOutputDates.sort()

    NumDate = len(AllCopolFile)

    OutputSuffix = 'Temporal_Average_' + UniqueOutputDates[0] + '_' + UniqueOutputDates[-1] + '.tif'

	# Apply average to Copol
    OutputCopol = os.path.join(aInputFolder, 'VV_' + OutputSuffix)
    OTBAverage(AllCopolFile, OutputCopol, aRam)

	# Apply filtering to crosspol
    OutputCrosspol = os.path.join(aInputFolder, 'VH_' + OutputSuffix)
    OTBAverage(AllCrosspolFile, OutputCrosspol, aRam)

	# Generate ratio intensity or convert in dB and creat vrt for color composition
	# List all folders in Time Filtering forlder
	# Get full path of time filtering folder
    if aOutput_in_dB:
        OutVrtFile = GenerateDualPolColorcompositiondBSingle(OutputCopol, OutputCrosspol, aRam)
		
    else:
        OutVrtFile = GenerateDualPolColorcompositionIntSingle(OutputCopol, OutputCrosspol, aRam)
		
	# Convert to envi format
	# print 'OutVrtFile',OutVrtFile
    GDALConvertFormat(OutVrtFile, OutVrtFile.replace('.vrt', '.hdr'), aRam)
	
	# Del file from vrt
    FilesFromVrtTodelete = GetFilesFromVrt(OutVrtFile)
    for vrt in FilesFromVrtTodelete:
        os.remove(vrt)
		
	# Remove vrt
    if os.path.exists(OutVrtFile):
        os.remove(OutVrtFile)
		

def GenerateDualPolColorcompositionIntSingle(aCopolFile, aCrosspolFile):
	Ratio = aCopolFile.replace('VV_', 'VH-VV_')
	# print Ratio

	OutputColorCompVRTFile = aCopolFile.replace('VV_',  'VV_VH_VH-VV_')
	OutputColorCompVRTFile = OutputColorCompVRTFile.replace('.tif',  '.vrt')

	# print OutputColorCompVRTFile

	RatioDualPol(aCopolFile, aCrosspolFile, Ratio, Ram)

	GdalBuildVRT([aCopolFile, aCrosspolFile, Ratio], OutputColorCompVRTFile,  0, 0)
	
	return OutputColorCompVRTFile


def GDALConvertFormat(aInputRaster, aOutputRaster, aRam):

    cmd = "gdal_translate -of ENVI "
    cmd +=  str(aInputRaster)
    cmd += " " + str(aOutputRaster).replace('.hdr',  '')
    cmd += " -ot Float32"

    print('cmd \n',cmd)


	#-in QB_Toulouse_Ortho_XS.tif -out otbConvertWithScalingOutput.png uint8

	
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OTBAverage(aInputRasterList, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    for rast in aInputRasterList:
        cmd += rast + ' '
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputRaster
    cmd += " -exp "
    cmd += "\"avg("
    for i in range(len(aInputRasterList)):
        cmd += 'im' + str(i+1) + 'b1,'
    cmd = cmd[:-1]
    cmd += ")\""

    # print cmd
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

'''
This function convert S1 intensity to dB
'''
def Int2dB(aInputFile, aBand, aOutputFile, aRam):
    cmd = "otbcli_BandMath -il "
    cmd += aInputFile + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputFile + "\""
    cmd += " -exp "
    cmd += "\"" + aBand + "<=0.001 ? -30. : 10. * log10(" +aBand +")\""

    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


def GenerateDualPolColorcompositionInt(aSubfolders, aOutputFolder, aRam):
    # Loop thru different S1 data (folder)
    for Folder in aSubfolders:
        # List all tif and tiff files
        AllTifFile = GetFileByExtensionFromDirectory(Folder, 'tif')

        InDirName = os.path.split(Folder)[1]

        # Create Output subfolder
        OutFolder = modOsJoinPath([aOutputFolder,InDirName])
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)

        Dual = False
        if '1SDV' in Folder:
            CopolFile = [s for s in AllTifFile if "VV" in s][0]
            CrosspolFile = [s for s in AllTifFile if "VH" in s][0]
            Dual = True

        elif '1SDH' in Folder:
            CopolFile = [s for s in AllTifFile if "HH" in s][0]
            CrosspolFile = [s for s in AllTifFile if "HV" in s][0]
            Dual = True
        else:
            print('Not dual pol data')

        if Dual:
            CopolFileName = os.path.basename(os.path.splitext(CopolFile)[0])
            CrosspolFileName = os.path.basename(os.path.splitext(CrosspolFile)[0])

            if '1SDV' in Folder:
                BaseNameFile = CopolFileName.replace('VV','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                Ratio = modOsJoinPath([OutFolder,  BaseNameFile + '_VH-VV' +'.tif'])

                OutputColorCompVRTFile = modOsJoinPath([OutFolder,  BaseNameFile + 'VV_VH_VH-VV' +'.vrt'])
            else:
                BaseNameFile = CopolFileName.replace('HH','')
                BaseNameFile = os.path.basename(os.path.splitext(BaseNameFile)[0])

                Ratio = modOsJoinPath([OutFolder,  BaseNameFile + 'HV-HH' +'.tif'])

                OutputColorCompVRTFile = modOsJoinPath([OutFolder,  BaseNameFile + 'HH_HV_HV-HH' +'.vrt'])

            RatioDualPol(CopolFile, CrosspolFileName, Ratio, aRam)



            # VRT color composition
            GdalBuildVRT([CopolFileName, CrosspolFileName, Ratio], OutputColorCompVRTFile,  0, 0)
'''
This function generate the ratio between intensities
'''
def RatioDualPol(aInputFileCopol, aInputFileCrosspol, aOutputFile, aRam):
    cmd = "otbcli_BandMath -il "
    cmd += aInputFileCopol + " "
    cmd += aInputFileCrosspol + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputFile + "\""
    cmd += " -exp "
    cmd += "\" im2b1 / im1b1 \""

    # progress.setInfo(cmd)
    # print cmd
    
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

'''
This function apply lee filtering data to all files in a folder
'''
def ApplyLeePreFiltering(aFolderList, aOutputFolder, aWindowSize, aENL, aRam):
    Radius = int(aWindowSize / 2.)
    # Loop thru different S1 data (folder)
    for Folder in aFolderList:
        # List all tif and tiff files
        AllTifFile = GetFileByExtensionFromDirectory(Folder, 'tif')

        InDirName = os.path.split(Folder)[1]

        # Create Output subfolder
        OutFolder = modOsJoinPath([aOutputFolder,InDirName])
        if not os.path.exists(OutFolder):
            os.makedirs(OutFolder)

        for file in AllTifFile:
            FileName = os.path.basename(os.path.splitext(file)[0])
            OutputFile = modOsJoinPath([OutFolder,  FileName + '_SpkLee_W' + str(aWindowSize) + '_NL' + str(aENL) +'.tif'])

            OTBLeeFiltering(file,OutputFile, Radius, aENL, aRam)


'''
This function use OTB to apply Lee Filtering
'''
def OTBLeeFiltering(aInputFile,aOutputFile, aLeeRadius, aENL, aRam):
    cmd = "otbcli_Despeckle"
    cmd += " -in " + aInputFile
    cmd += " -filter lee"
    cmd += " -filter.lee.rad " + str(aLeeRadius)
    cmd += " -filter.lee.nblooks " + str(aENL)
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputFile + "?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES\""

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
'''
This function control that the input vector contain one different path per polygon
if not  stop and return an alert
In addition it control that the Path user request is compliant with path in vector
'''
def CheckAllDifferentRelativeOrbit(aInputShape,aPathFieldName, aUserPathList):
    ExceptionMessage = ''
    # Initiate Boolean alert
    isAllUniquePath = False
    isUserPathCompliantShape = True
    # Convert to string due to Windows problem
    aPathFieldName = str(aPathFieldName)
    
    # Open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop thru feature and get path value
    AllPathList = []
    for feature in layer:
        AllPathList.append(feature.GetField(aPathFieldName))
    
    # Close the source
    dataSource = None
    
    # Check if we have all different Path
    UniquePath = list(set(AllPathList))
    if len(AllPathList) == len(UniquePath):
        isAllUniquePath = True
    else:
        ExceptionMessage += 'You not have all different Path in your shape, '
        logging.info('You not have all different Path in your shape')
    
    # Check if Path user request correspond to the shape
    for UserPath in aUserPathList:
        if UserPath not in AllPathList:
            isUserPathCompliantShape = False
            ExceptionMessage += 'You ask a path not in your shape orbit ' + str(UserPath)
            logging.info('You ask a path not in your shape orbit ' + str(UserPath))
    
    # Exception
    if not isAllUniquePath or not isUserPathCompliantShape:
        raise Exception(ExceptionMessage)

'''
This function return the list path files that intersect the study area AND
that are in the required orbit
'''
def getS1ByTile(aInputShape,rawRasterList, aPathFieldName, aPath):
    # Initialization of current Path
    PolygonPath = 0
    
    # Convert to string due to Windows problem
    aPathFieldName = str(aPathFieldName)
    
    # We open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop Thru feature
    for currentPath in layer: 
        if int(currentPath.GetField(aPathFieldName))==int(aPath):
            PolygonPath = aPath
            break
        
    # We get the geometry of the user polygon
    pathFootPrint = currentPath.GetGeometryRef()
    
    intersectRaster=[]
    for image in rawRasterList:
        # We get relative orbit
        RasterRelOrbit = getRelativeOrbit(image)
        
        NW,NE,SE,SW = getOrigin(image)

        # We create a new empty geometry to store the S1 raw footprint
        poly = ogr.Geometry(ogr.wkbPolygon)
        
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(NW[1], NW[0],0)
        ring.AddPoint(NE[1], NE[0],0)
        ring.AddPoint(SE[1], SE[0],0)
        ring.AddPoint(SW[1], SW[0],0)
        ring.AddPoint(NW[1], NW[0],0)
        
        poly.AddGeometry(ring)
        
        # Generate intercection between user polygon and curent S1 footprint
        intersection = poly.Intersection(pathFootPrint)

        # test if we have intercection
        if intersection.GetArea()!=0 and RasterRelOrbit == PolygonPath:
            intersectRaster.append(image)
            
        poly = None
        ring = None
        
    dataSource = None
    
    return intersectRaster  

def ExtractZipFileFromPartFileName(aZipPath, aStr,aDestPath):
    try:
        archive = zipfile.ZipFile(aZipPath, 'r')
        if archive.testzip() is not None:
            logging.info('Corrupted ZIP ' + aZipPath)
        else:
            zipinfos = archive.infolist()

            for zipinfo in zipinfos:
                if aStr in zipinfo.filename:
                    source = archive.open(zipinfo.filename)
                    target = open(aDestPath, "wb")
                    with source, target:
                        shutil.copyfileobj(source, target)

            archive.close()
            logging.info('File from ZIP well extracted :' + aDestPath)

    except  zipfile.BadZipfile:
        logging.info('Bad ZIP ' + aZipPath)

def CheckZipIntercection(aInputShape,zipS1, aPathFieldName, aPath):
    import zipfile
    # Initialization check zip
    CheckZip = False
    
    # Convert to string due to Windows problem
    aPathFieldName = str(aPathFieldName)
    
    # We open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop Thru feature
    for currentPath in layer: 
        if int(currentPath.GetField(aPathFieldName))==int(aPath):
            PolygonPath = aPath
            break
        
    # We get the geometry of the user polygon
    pathFootPrint = currentPath.GetGeometryRef()

    # Unzip the manifest.safe file in temporary folder
    image = zipS1.replace('.zip', '_manifest.safe')
    ExtractZipFileFromPartFileName(zipS1, 'manifest.safe',image)

    # We get relative orbit
    RasterRelOrbit = getRelativeOrbit(image)
    
    NW,NE,SE,SW = getOrigin(image)

    # We create a new empty geometry to store the S1 raw footprint
    poly = ogr.Geometry(ogr.wkbPolygon)
    
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(NW[1], NW[0],0)
    ring.AddPoint(NE[1], NE[0],0)
    ring.AddPoint(SE[1], SE[0],0)
    ring.AddPoint(SW[1], SW[0],0)
    ring.AddPoint(NW[1], NW[0],0)
    
    poly.AddGeometry(ring)
    
    # Generate intercection between user polygon and curent S1 footprint
    intersection = poly.Intersection(pathFootPrint)

    # test if we have intercection
    if (intersection.GetArea()!=0):
        CheckZip = True
        
    poly = None
    ring = None

    if os.path.exists(image):
        os.remove(image)
        
    dataSource = None
    layer = None
    pathFootPrint = None
    intersection = None


    return CheckZip

'''
This function return the list path files that intersect the study area AND
that are in the required orbit
'''
def getS1ZipByTile(aInputShape,rawZipPathList, aPathFieldName, aPath):
    import zipfile
    # Initialization of current Path
    PolygonPath = 0
    
    # Convert to string due to Windows problem
    aPathFieldName = str(aPathFieldName)
    
    # We open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop Thru feature
    for currentPath in layer: 
        if int(currentPath.GetField(aPathFieldName))==int(aPath):
            PolygonPath = aPath
            break
        
    # We get the geometry of the user polygon
    pathFootPrint = currentPath.GetGeometryRef()
    
    intersectRaster=[]
    for zipS1 in rawZipPathList:
        # Unzip the manifest.safe file in temporary folder
        image = zipS1.replace('.zip', '_manifest.safe')
        ExtractZipFileFromPartFileName(zipS1, 'manifest.safe',image)

        # We get relative orbit
        RasterRelOrbit = getRelativeOrbit(image)
        
        NW,NE,SE,SW = getOrigin(image)

        # We create a new empty geometry to store the S1 raw footprint
        poly = ogr.Geometry(ogr.wkbPolygon)
        
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(NW[1], NW[0],0)
        ring.AddPoint(NE[1], NE[0],0)
        ring.AddPoint(SE[1], SE[0],0)
        ring.AddPoint(SW[1], SW[0],0)
        ring.AddPoint(NW[1], NW[0],0)
        
        poly.AddGeometry(ring)
        
        # Generate intercection between user polygon and curent S1 footprint
        intersection = poly.Intersection(pathFootPrint)

        # test if we have intercection
        if intersection.GetArea()!=0 and RasterRelOrbit == PolygonPath:
            intersectRaster.append(zipS1)
            
        poly = None
        ring = None

        if os.path.exists(image):
            os.remove(image)
        
    dataSource = None
    
    return intersectRaster  

'''
This function call ogr in system command to reproject a vector
'''
def ReprojVector(aInputVector, aOutputVector, aEPSG):
    CmdList = [
        'ogr2ogr -overwrite',
        '-f \"ESRI Shapefile\" %s'     % aOutputVector,
        '%s' % aInputVector,
        '-t_srs EPSG:%s'     % str(aEPSG)
    ]
    p1 = subprocess.Popen (' '.join(CmdList), shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


'''
This function rasterize a vector using pixel size parameter
'''
def Rasterize(aInputVector, aOutputRaster, aOutPixelSize):

    aOutPixelSize = float(aOutPixelSize)

    # Get a Layer's Extent
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aInputVector, 0)
    print('Rasterize :   aInputVector, aOutputRaster, aOutPixelSize', aInputVector, aOutputRaster, aOutPixelSize)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent() # (X min, X max,y min,y max)

    # Close DataSource
    inDataSource.Destroy()

    xmin = round(extent[0])
    ymin = round(extent[2])
    xmax = round(extent[1])
    ymax = round(extent[3])

    # print '\nBefore xmin, ymin, xmax, ymax', xmin, ymin, xmax, ymax

    # compute num col and row
    cols = round( abs(xmin - xmax)/ aOutPixelSize)
    rows = round( abs(ymin - ymax)/ aOutPixelSize)

    # print '\n Cols rows ', cols, rows

    xmax = xmin + cols * aOutPixelSize
    ymax = ymin + rows * aOutPixelSize


    # print '\nAfter xmin, ymin, xmax, ymax', xmin, ymin, xmax, ymax



    cmd = "gdal_rasterize "
    cmd += " -burn 1 "
    cmd += " -of GTiff "
    cmd += " -a_nodata 0 "
    cmd += " -tr " + str(aOutPixelSize) + " "+ str(aOutPixelSize)+ " "
    cmd += " -te " + str(xmin) + " "+ str(ymin) + " " + str(xmax) + " "+ str(ymax)
    cmd += " -ot Byte "
    cmd += ' -co COMPRESS=DEFLATE -co PREDICTOR=2  -co BIGTIFF=YES '
    cmd += aInputVector + " "
    cmd += aOutputRaster

    # print cmd
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
   
'''
This function extract the user polygon using the path number
'''
def CreateShapeFromPath(aInputShape,aFieldName, aFieldValue, aOutputShape):
    # Convert to string due to Windows problem
    aFieldName = str(aFieldName)
    
    # Open shape file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop thru polygon
    for currentPath in layer:
        if currentPath.GetField(aFieldName)==aFieldValue:
            break
    pathFootPrint = currentPath.GetGeometryRef()
    
    # Save extent to a new Shapefile
    outShapefile = aOutputShape
    LayerName = os.path.basename(os.path.splitext(aOutputShape)[0])
    LayerName = LayerName.encode('utf-8')


    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    
    # Remove output shapefile if it already exists
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)
    

    # Create the output shapefile
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    outDataSource = outDriver.CreateDataSource(outShapefile)
    # progress.setInfo('LayerName' + LayerName)
    outLayer = outDataSource.CreateLayer(str(LayerName), srs, ogr.wkbPolygon)
    # progress.setInfo('LayerName APRES' + LayerName)
    
    # Create the feature and set values
    featureDefn = outLayer.GetLayerDefn()
    feature = ogr.Feature(featureDefn)
    feature.SetGeometry(pathFootPrint)
    outLayer.CreateFeature(feature)
    feature = None
    
    outDataSource = None
    dataSource = None
        
    # Write the prj file
    OutPrjFile = os.path.splitext(aOutputShape)[0] + '.prj'
    srs.MorphToESRI()
    filePrj = open(OutPrjFile, 'w')
    filePrj.write(srs.ExportToWkt())
    filePrj.close()

'''
This function get the coordinate origin from S1 manifeste file
'''
def getOrigin(aManifestFile):
    with open(aManifestFile,"r") as saveFile:
        for line in saveFile:
            if "<gml:coordinates>" in line:
                coor = line.replace("                <gml:coordinates>","").replace("</gml:coordinates>","").split(" ")
                coord = [(float(val.replace("\n","").split(",")[0]),float(val.replace("\n","").split(",")[1]))for val in coor]

    return coord[0],coord[1],coord[2],coord[3]

'''
This function get the name of the data withour .safe
'''
def getBaseDataNameFromS1Folder(aFolderName):
    return '_'.join(aFolderName.split("/")[-1].split("_")[:5])

'''
This function proces the S1 GRD data
'''
def ProcessS1Dataset(aInputManifestList, aOutputDir, aInputShape, aDemDir, aResolution,
                     aCalibration_Type, aNoise, aCalibName, aCalibType,
                     aEPSG, aRam):
                         
    # Create tmp dir
    # Get time
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    TmpDir = modOsJoinPath([aOutputDir, 'tmp' + TimeNow])
    if not os.path.exists(TmpDir):
        os.makedirs(TmpDir)
        
    # Rasterize the polygone if enable option
    # Repro Vector to EPSG 3857
    ReprojVectorPath = modOsJoinPath([TmpDir,'ReprojVector.shp'])
    ReprojVector(aInputShape, ReprojVectorPath, 3857)
    ClipRasterBase = modOsJoinPath([aOutputDir,'RasterArea.tif'])
    Rasterize(ReprojVectorPath, ClipRasterBase, 100)


    # List all SENTINEL-1 sub directories
    S1DataFolders = [ os.path.dirname(ManFile) for ManFile in aInputManifestList]
 
    # Loop thru different S1 data (folder)
    for Folder in S1DataFolders:
        TiffFiles = GetFileByExtensionFromDirectory(Folder, 'tiff')
        
        SafeName = os.path.split(Folder)[-1]
        FolderName = SafeName.split('.')[0]
        SatelliteFolder = FolderName.split('_')[0]
        DateFolder = FolderName.split('_')[4]
        DateFolder=DateFolder[0:DateFolder.find('T')]
        
        WorkingFolder = modOsJoinPath([aOutputDir,FolderName])
        if not os.path.exists(WorkingFolder):
            os.makedirs(WorkingFolder)
        
        for TiffFile in TiffFiles:
            if 'grd-vh' in TiffFile:
                SuffixOrtho = '_VH_'

            if 'grd-hv' in TiffFile:
                SuffixOrtho = '_HV_'
            
            if 'grd-vv' in TiffFile:
                SuffixOrtho = '_VV_'

            if 'grd-hh' in TiffFile:
                SuffixOrtho = '_HH_'

        
            SuffixOrtho += aCalibName+'_Ortho.tif'

            OutClipedFile = modOsJoinPath([TmpDir,'S1Clip.tif'])
            OTBExtractRoi(TiffFile, ClipRasterBase, OutClipedFile, aRam)
            TiffFile = OutClipedFile

            OutputFile = modOsJoinPath([TmpDir,FolderName,FolderName + SuffixOrtho])
            if not os.path.exists(os.path.dirname(OutputFile)):
                os.makedirs(os.path.dirname(OutputFile))

            OutputFileCliped = modOsJoinPath([aOutputDir,FolderName,
                                            SatelliteFolder + '_' + DateFolder + SuffixOrtho])      

            # Do GRD conversion to Ortho (including calibration)
            GRD2Calib_Ortho(TiffFile,OutputFile,aDemDir, aCalibType, aNoise, aEPSG, ClipRasterBase, aRam, TmpDir)

            GdalClipRasterWithVector(OutputFile,aInputShape ,
                                     OutputFileCliped, aResolution,  0, aEPSG, aRam)
        
            if os.path.exists(OutputFile):
                os.remove(OutputFile)
            
    # Del Temporay Dir
    shutil.rmtree(TmpDir)
    if os.path.exists(ClipRasterBase):
        os.remove(ClipRasterBase)
        
    # Check if we need to mosaic concecutive data
    # List all folder
    SubFolders = get_immediate_subdirectories(aOutputDir)
    concatenateImage(SubFolders, aRam)

def GetFilesFromVrt(aInputVRT):
    #Open the VRT
    ds = gdal.Open(aInputVRT)

    # Get file name path
    RawVrtFileList = ds.GetFileList()[1:]

    ds = None

    if '.ovr' in RawVrtFileList[0]:
        RawVrtFileList = RawVrtFileList[1:]

    return RawVrtFileList

'''
This function proces the S1 GRD data
'''
def ProcessS1ZipDataset(aInputZipList, aOutputDir, aInputShape, aDemDir, aResolution,
                     aCalibration_Type, aNoise, aCalibName, aCalibType,
                     aEPSG, aPrctThr, aRam):
                         
    # print 'aInputShape \n', aInputShape
    EPSGCode = aEPSG.split(':')[1]

    # Create tmp dir
    # Get time
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    TmpDir = modOsJoinPath([aOutputDir, 'tmp' + TimeNow])
    if not os.path.exists(TmpDir):
        os.makedirs(TmpDir)
        
    # Rasterize the polygone if enable option
    # Repro Vector to EPSG 3857
    ReprojVectorPath = modOsJoinPath([TmpDir,'ReprojVector.shp'])
    ReprojVector(aInputShape, ReprojVectorPath, int(EPSGCode))
    ClipRasterBase = modOsJoinPath([aOutputDir,'RasterArea.tif'])
    Rasterize(ReprojVectorPath, ClipRasterBase, aResolution)


    # List all SENTINEL-1 sub directories
    S1DataFolders = [ os.path.dirname(ManFile) for ManFile in aInputZipList]
 
    # Loop thru different S1 data (folder)
    for zipFile in aInputZipList:
        # Get file name
        FolderName = os.path.basename(os.path.splitext(zipFile)[0])
        DirFile =  os.path.dirname(zipFile)
        # FolderName = os.path.split(DirFile)[1]
        TmpExtractDir = modOsJoinPath([DirFile,'Tmp'])
        if not os.path.exists(TmpExtractDir):
            os.makedirs(TmpExtractDir)

        # print 'FolderName \n',FolderName
        # print 'DirFile \n',DirFile
        # print 'zipName \n',zipName
        # extract the data
        #print 'Before extract full \n',zipFile,TmpExtractDir
        #print f3(zipFile, TmpExtractDir)
        ExtractFullZipFile(zipFile,TmpExtractDir)
        

        TiffFiles = GetFileByExtensionFromDirectory(TmpExtractDir, 'tiff')
        
        # SafeName = os.path.split(Folder)[-1]
        SatelliteFolder = FolderName.split('_')[0]

        DateFolder = FolderName.split('_')[4]
        DateFolder=DateFolder[0:DateFolder.find('T')]
        
        WorkingFolder = modOsJoinPath([aOutputDir,FolderName])
        if not os.path.exists(WorkingFolder):
            os.makedirs(WorkingFolder)
        
        for TiffFile in TiffFiles:
            if 'grd-vh' in TiffFile:
                SuffixOrtho = '_VH_'

            if 'grd-hv' in TiffFile:
                SuffixOrtho = '_HV_'
            
            if 'grd-vv' in TiffFile:
                SuffixOrtho = '_VV_'

            if 'grd-hh' in TiffFile:
                SuffixOrtho = '_HH_'

        
            SuffixOrtho += aCalibName+'_Ortho.tif'

            OutClipedFile = modOsJoinPath([TmpDir,'S1Clip.tif'])
            OTBExtractRoi(TiffFile, aInputShape, OutClipedFile, aRam)
            TiffFile = OutClipedFile

            OutputFile = modOsJoinPath([TmpDir,FolderName,FolderName + SuffixOrtho])
            if not os.path.exists(os.path.dirname(OutputFile)):
                os.makedirs(os.path.dirname(OutputFile))

            OutputFileCliped = modOsJoinPath([aOutputDir,FolderName,
                                            SatelliteFolder + '_' + DateFolder + SuffixOrtho])      

            # Do GRD conversion to Ortho (including calibration)
            GRD2Calib_Ortho(TiffFile,OutputFileCliped,aDemDir, aCalibType, aNoise, aEPSG, ClipRasterBase, aRam, TmpDir)

            # GdalClipRasterWithVector(OutputFile,aInputShape ,
                                     # OutputFileCliped, aResolution,  0, aEPSG, aRam)
        
            if os.path.exists(OutputFile):
                os.remove(OutputFile)
        # Del Temporay Dir
        shutil.rmtree(TmpExtractDir)
            
    # Del Temporay Dir
    shutil.rmtree(TmpDir)
    
        
    # Check if we need to mosaic concecutive data
    # List all folder
    SubFolders = get_immediate_subdirectories(aOutputDir)
    concatenateImage(SubFolders, ClipRasterBase, aRam)
    # MosTiffFiles = GetFileByExtensionFromDirectory(aOutputDir, 'tif')
    # MosTiffFilesVH = [ item for item in MosTiffFiles if ('_VH_' or '_HV_') in item ]
    # logging.info('Control if ortho cover a least ' + str(aPrctThr) + ' Percentage of study area')
    # for vhFile in MosTiffFilesVH:
    #     ValidArea, AreaRef, AreaIntercect, PrctArea = TestRasterFootprint(vhFile,aInputShape,aPrctThr, 0.)
    #     logging.info('File ' + vhFile + ' Percentage of study area : ' + str(PrctArea))
    #     if not ValidArea:
    #         shutil.rmtree(os.path.dirname(vhFile))
    #         logging.info('Removed invalid percantage file ' + vhFile)
    
    

    # if os.path.exists(ClipRasterBase):
    #     os.remove(ClipRasterBase)


# Function that update a shape to have valid feature
# Be carefull it update the shape so can be changed
def ValidShapeFile(aInputShape):
    gk = ogr.Open(
        aInputShape, 1) # <====== 1 = update mode
    gk_lyr = gk.GetLayer()

    for feature in gk_lyr:
        geom = feature.GetGeometryRef()
        if not geom.IsValid():
            feature.SetGeometry(geom.Buffer(0)) # <====== SetGeometry
            gk_lyr.SetFeature(feature) # <====== SetFeature
            assert feature.GetGeometryRef().IsValid() # Doesn't fail

    gk_lyr.ResetReading()
    assert all(feature.GetGeometryRef().IsValid() for feature in gk_lyr)  # Doesn't fail


'''
This function return the list path files that intersect the study area AND
that are in the required orbit
'''
def getZipInterectPolyg(aInputShape,aFootprintShape,rawZipPathList, aPathFieldName, aPath, aDateField, aPrctThr):
    import zipfile
    # Initialization of current Path
    PolygonPath = 0
    
    # Convert to string due to Windows problem
    aPathFieldName = str(aPathFieldName)
    
    # We open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aInputShape, 0)
    layer = dataSource.GetLayer()
    
    # Loop Thru feature
    for currentPath in layer: 
        if int(currentPath.GetField(aPathFieldName))==int(aPath):
            PolygonPath = aPath
            break
        
    # We get the geometry of the user polygon
    pathFootPrint = currentPath.GetGeometryRef()

    # We test all polygon from footprint file to know the relavant date to process
    # Open the footprint layer
    dataSourceFootprint = driver.Open(aFootprintShape, 0)
    layerFootprint = dataSourceFootprint.GetLayer()
    # List that contain that with coverage of study area greater or equal to threashold
    RelevantDateList = []
    # Loop thru polygon
    for featFootprint in layerFootprint:
        featDate = featFootprint.GetField(aDateField)
        featGeomFootPrint = featFootprint.GetGeometryRef()

        # Generate intercection between user polygon and curent S1 footprint
        intersection = pathFootPrint.Intersection(featGeomFootPrint)

        AreaIntercect = intersection.GetArea()
        AreaRef = pathFootPrint.GetArea()
        
        PrctArea = AreaIntercect / AreaRef * 100.

        # logging.info('Date ' + str(featDate) + ' have a percentage cover with study area of ' + str(PrctArea))

        Selected = 'Not selected'
        if PrctArea >= aPrctThr:
            RelevantDateList.append(featDate)
            Selected = 'Selected'
        
        logging.info('Date ' + str(featDate) + ' have a percentage cover with study area of ' + str(round(PrctArea)) + '  ' + Selected)
        
        featFootprint = None
    dataSource = None
    dataSourceFootprint = None

    intersectRaster=[]
    for zipS1 in rawZipPathList:
        zipDate = getDayFromS1FileOrFolder(zipS1)



        if zipDate in RelevantDateList:
            CheckZip = CheckZipIntercection(aInputShape,zipS1, aPathFieldName, aPath)
            if CheckZip:
                intersectRaster.append(zipS1)

    return intersectRaster  

# Dissolve a shape using dedicated field
def DissolveField(aInputFile, aOutputFile, aDissolveField):
    # ogr2ogr output.shp input.shp -dialect sqlite -sql "SELECT ST_Union(geometry), dissolve_field FROM input GROUP BY dissolve_field"
    # 
    InputFileName = os.path.basename(os.path.splitext(aInputFile)[0])
    
    cmd = "ogr2ogr  "
    cmd += aOutputFile + " "
    cmd += aInputFile + " "
    cmd += " -dialect sqlite -sql \"SELECT ST_Union(ST_buffer(Geometry,0.0005))," + aDissolveField
    cmd += " FROM " + InputFileName + " GROUP BY " + aDissolveField + " \""

    print(cmd)
    
    # progress.setInfo('\nDissolveAllFeatures ' + cmd)
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


# From S1 zip file it create a shape file of data footprint with date addition field
def CreateShapeOfAllData(aInzip, aOutputShape, aDateField, aTileField):
    # create output shape
    # Convert to string due to Windows problem
    aDateField = str(aDateField)
    aTileField = str(aTileField)

    LayerName = os.path.basename(os.path.splitext(aOutputShape)[0])
    LayerName = LayerName.encode('utf-8')

    outDriver = ogr.GetDriverByName("ESRI Shapefile")

    # Remove output shapefile if it already exists
    if os.path.exists(aOutputShape):
        outDriver.DeleteDataSource(aOutputShape)

    # create the spatial reference, WGS84
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    # Create the output shapefile
    outDataSource = outDriver.CreateDataSource(aOutputShape)
    out_lyr_name = os.path.splitext( os.path.split( aOutputShape )[1] )[0]
    outLayer = outDataSource.CreateLayer(out_lyr_name, srs, ogr.wkbMultiPolygon)

    # Add date and relative orbit field
    # Relative orbite
    TileField = ogr.FieldDefn(aTileField, ogr.OFTInteger)
    outLayer.CreateField(TileField)

    # Date Field
    DateField = ogr.FieldDefn(aDateField, ogr.OFTString)
    DateField.SetWidth(10)
    outLayer.CreateField(DateField)

    # Loop zips
    count = 0
    for zipS1 in aInzip:
        # Unzip the manifest.safe file in temporary folder
        image = zipS1.replace('.zip', '_manifest.safe')
        ExtractZipFileFromPartFileName(zipS1, 'manifest.safe',image)

        # Get relative orbit
        RasterRelOrbit = getRelativeOrbit(image)

        # Get date
        Date = getDayFromS1FileOrFolder(image)
        # Remove None element if there is
        # Date = [i for i in Date if i]

        # Get footprint
        NW,NE,SE,SW = getOrigin(image)

        # We create a new empty geometry to store the S1 raw footprint
        poly = ogr.Geometry(ogr.wkbPolygon)
        
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(NW[1], NW[0],0)
        ring.AddPoint(NE[1], NE[0],0)
        ring.AddPoint(SE[1], SE[0],0)
        ring.AddPoint(SW[1], SW[0],0)
        ring.AddPoint(NW[1], NW[0],0)
        
        poly.AddGeometry(ring)

        # Add Feature
        # create the feature
        featureDefn=outLayer.GetLayerDefn()
        feature = ogr.Feature(featureDefn)
        # pdb.set_trace()
        # Set the attributes using the values from the delimited text file
        feature.SetField(aDateField, Date)
        
        feature.SetField(aTileField, RasterRelOrbit)

        # Set the feature geometry using the point
        feature.SetGeometry(poly)
        # Create the feature in the layer (shapefile)
        outLayer.CreateFeature(feature)
        # Dereference the feature
        feature = None

        poly = None
        ring = None

        # remove manifest.safe
        if os.path.exists(image):
            os.remove(image)


        

    outLayer = None
    
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    
    # Write the prj file
    OutPrjFile = os.path.splitext(aOutputShape)[0] + '.prj'
    srs.MorphToESRI()
    filePrj = open(OutPrjFile, 'w')
    filePrj.write(srs.ExportToWkt())
    filePrj.close()

'''
This function clip rax sentinel-1 data using reference raster (use it extent)
'''
def OTBExtractRoi(aInputFile, aInputRefFile, aOutputFile, aRam):
    cmd = "otbcli_ExtractROI -in "
    cmd += aInputFile + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputFile
    cmd += " -mode fit "
    cmd += " -mode.fit.vect " + aInputRefFile

    # progress.setInfo(cmd)
    #print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    

'''
This function run calibration and next orthorectification
'''
def GRD2Calib_Ortho(aInputFile,aOutputFile, aDemDir, aCalibration_Type, aNoise, aEPSG, aRefOrtho ,aRam, aTmpDir):
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    # Output Intermediate Calibrated file
    OutputCalibratedFile = modOsJoinPath([aTmpDir, TimeNow + "_Calib.tif"])

    # Calibration
    OTBSARCalibration(aInputFile, OutputCalibratedFile, aNoise, aCalibration_Type, aRam)

    # Orthorectification
    OTBOrthorectification(OutputCalibratedFile, aOutputFile, aDemDir, aEPSG, aRefOrtho, aRam)

    if os.path.exists(OutputCalibratedFile):
        os.remove(OutputCalibratedFile)
            

'''
This function calibrate SAR file
aNoise   --> to remove (or not) noise  - True or False
aCalibration_Type    --> to chose aCalibration_Type      - sigma/gamma/beta/dn
'''
def OTBSARCalibration(aInputFile, aOutputFile, aNoise, aCalibration_Type, aRam):
    cmd = "otbcli_SARCalibration -in "
    cmd += aInputFile + " "
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += aOutputFile
    cmd += " -removenoise " + aNoise
    cmd += " -lut " + aCalibration_Type
 
    # progress.setInfo(cmd)
    # print cmd
 
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    
'''
This function do orthorectification of any optical/radar file
aDEMDir  --> directory that contain DEM

opt.gridspacing is not in parameter function but need to be near 4 to 10 times of output pixel size
For example, with 10m output pixel size, you can chose bettween 40 to 100.
More less is the value more accurate is the results but more long
In addition this value have to be linked to DEM pixel size
'''
def OTBOrthorectification(aInputFile, aOutputFile, aDEMDir, aEPSG, aRefOrtho , aRam):
    # Boolean to know if in qgis
    try:
        import qgis.utils
        inqgis = qgis.utils.iface is not None
    except ImportError:
        no_requests = True
        inqgis = False

    if inqgis:
        QGISProcessingScriptFolder = getQGISProcessingScriptFolder()
        
        # Create the S1Lib lib folder path
        GeoidPath = modOsJoinPath([QGISProcessingScriptFolder, 'Data', 'Geoid', 'egm96.grd'])
    else:
        LocalScriptFileDir = os.path.dirname(os.path.abspath((getsourcefile(lambda:0)))).replace("\\","/")
        FolderLevelUp = os.path.dirname(LocalScriptFileDir)
        GeoidPath = modOsJoinPath([FolderLevelUp, 'Data', 'Geoid', 'egm96.grd'])
    
    EPSGCode = aEPSG.split(':')[1]

    cmd = "otbcli_OrthoRectification -io.in "
    cmd += aInputFile + " "
    cmd += " -opt.ram " + str(aRam)
    cmd += " -io.out "
    cmd += aOutputFile
    cmd += " -elev.dem " + aDEMDir
    cmd += " -elev.geoid " + GeoidPath
    cmd += " -opt.gridspacing 40 "
    cmd += " -map epsg "
    cmd += " -map.epsg.code " + EPSGCode
    cmd += " -outputs.mode orthofit "
    cmd += " -outputs.ortho "  +aRefOrtho


    # progress.setInfo(cmd)
    #print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def GdalClipRasterWithVector(aInputRaster, aInputVector, aOutputFile, aOutputResolution, aSRCNoData, aOutEPSG, aRam):
    cmd = "gdalwarp "
    cmd += "--config GDAL_CACHEMAX " + str(aRam) + " -multi -wo NUM_THREADS=val/ALL_CPUS "
    cmd += " -tr " + str(aOutputResolution) + ' ' + str(aOutputResolution)+ ' '
    cmd += " -t_srs " + str(aOutEPSG)
    cmd += " -r average -q -multi -crop_to_cutline -cutline "
    cmd += aInputVector
    # cmd += ' -co COMPRESS=DEFLATE -co PREDICTOR=3  -co BIGTIFF=YES  -co TILED=YES '
    cmd += " -dstnodata " + str(aSRCNoData)
    cmd += " -of GTiff " + aInputRaster + ' ' + aOutputFile

    # progress.setInfo(cmd)
    # print cmd

    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]
    

'''
This function list this immediate subdirectories in one given directory
'''
def get_immediate_subdirectories(a_dir):
    return [modOsJoinPath([a_dir, name]) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]
                
                
def concatenateImage(aInputFolderList, aMask, aRam):
    # MainFolder = 
    FirstFile = aInputFolderList[0]

    MainFolder =  list(os.path.split(FirstFile)[:-1])[0]
    aInputFolderList.sort()

    # List all date
    AcqDateList = [getDayFromS1FileOrFolder(FolderName) for FolderName in aInputFolderList]
    # Remove None element if there is
    AcqDateList = [i for i in AcqDateList if i]


    UniqueAcqDateList = list(set(AcqDateList))
    for AcDate in UniqueAcqDateList:
        FileToConcatenate = [folder  for folder in aInputFolderList if AcDate in folder]
        if len(FileToConcatenate) >= 2:
            # Create new Directory (same name without hour)
            SFolder = os.path.split(FileToConcatenate[0])
            SFolder = SFolder[-1].split('_')
            SFolder[4] = AcDate
            SFolder = '_'.join(SFolder[0:5])
            NewOutDir = modOsJoinPath([MainFolder,SFolder])
            if not os.path.exists(NewOutDir):
                os.makedirs(NewOutDir)

            # List all tiff file
            TiffFiles = []
            for FileToConcat in FileToConcatenate:
                TiffFiles += GetFileByExtensionFromDirectory(FileToConcat, '.tif')

            # List copol file (VV or HH)
            CopolFile = [ item for item in TiffFiles if ('_VV_' or '_HH_') in item ]
            FileName = os.path.basename(os.path.splitext(CopolFile[0])[0])
            FileName = os.path.split(FileName)
            FileName = FileName[-1].split('_')
            FileName[1] = AcDate
            FileName = '_'.join(FileName)


            OutputConcatCopolFile = modOsJoinPath([NewOutDir,FileName +'.tif'])
            # Concatenate Copol
            OTBConcatenate(CopolFile, aMask, OutputConcatCopolFile, aRam)

            # List crosspol file (VH or HV)
            CrosspolFile = [ item for item in TiffFiles if ('_VH_' or '_HV_') in item ]
            FileName = modOsJoinPath([os.path.splitext(CrosspolFile[0])[0]])
            FileName = os.path.split(FileName)
            FileName = FileName[-1].split('_')
            FileName[1] = AcDate
            FileName = '_'.join(FileName)


            OutputConcatCrosspolFile = modOsJoinPath([NewOutDir,FileName +'.tif'])
            # Concatenate Copol
            OTBConcatenate(CrosspolFile, aMask, OutputConcatCrosspolFile, aRam)

            # delete the 2 folders
            for file in CopolFile + CrosspolFile:
                if  os.path.exists(os.path.dirname(file)):
                    print('')
                    shutil.rmtree(os.path.dirname(file))
                    

def OTBConcatenate(aInputRasterList, aMask, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    for rast in aInputRasterList:
        cmd += rast + ' '
    cmd += aMask + ' '

    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputRaster + "?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES\""
    cmd += " -exp "
    cmd += "\"" + 'im' + str(len(aInputRasterList)+1) + 'b1==0?0:'
    cmd += "max("
    for i in range(len(aInputRasterList)):
        cmd += 'im' + str(i+1) + 'b1,'
    cmd = cmd[:-1]
    cmd += ")\""

    print(cmd)
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]

def OTBConcatenateOld(aInputRasterList, aOutputRaster, aRam):
    cmd = "otbcli_BandMath -il "
    for rast in aInputRasterList:
        cmd += rast + ' '
    cmd += " -ram " + str(aRam)
    cmd += " -out "
    cmd += "\"" + aOutputRaster + "?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES&gdal:co:TILED=YES\""
    cmd += " -exp "
    cmd += "\"max("
    for i in range(len(aInputRasterList)):
        cmd += 'im' + str(i+1) + 'b1,'
    cmd = cmd[:-1]
    cmd += ")\""

    #print cmd
    p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
    ret= p1.communicate()[1]


def RemoveOldestZipAndOrho(aNumber_Of_Date_To_Keep, aZipFolder, aOrthoFolder):
    AllOrthofiles = GetFileByExtensionFromDirectory(aOrthoFolder, 'tif')

    # Get all ortho date
    ListOrthoDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOrthofiles if 'TempProcStack' not in RastPath]
    # Remove None element if there is
    ListOrthoDates = [i for i in ListOrthoDates if i]
    
    # Get all unique dates
    UniqueOrthoDates = list(set(ListOrthoDates))
    UniqueOrthoDates.sort()
    
    # Get Dates from ZipFolder
    AllZipFiles = GetFileByExtensionFromDirectory(aZipFolder, 'zip')
    
    # test if required date are lower or equal to orthe number of date
    if aNumber_Of_Date_To_Keep > len(UniqueOrthoDates):
        aNumber_Of_Date_To_Keep = UniqueOrthoDates
    
    # get all output surbirectories
    SubFolders = get_immediate_subdirectories(aOrthoFolder)
    # delete zip files
    if aNumber_Of_Date_To_Keep < len(UniqueOrthoDates):
        NumDateToDel = len(UniqueOrthoDates) - aNumber_Of_Date_To_Keep
        for date in UniqueOrthoDates[0:NumDateToDel-1]:
            # list zip files and del if old date
            for fileZip in AllZipFiles:    
                # Del IntCopol
                if date in fileZip and os.path.exists(fileZip):
                    print('\nRemove zip ', fileZip)
                    os.remove(fileZip)
                    
            # list folder and delete if old
            for folder in SubFolders:
                # del folder
                if date in folder and os.path.exists(folder):
                    print('\nRemove Ortho folder ', folder)
                    shutil.rmtree(folder)

def RemoveOldestFolerFiles(aNumber_Of_Date_To_Keep, aInFolder, aOutFolder):
    AllOutfiles = GetFileByExtensionFromDirectory(aOutFolder, 'tif')

    # Get all ortho date
    ListOutDates = [getDayFromS1FileOrFolder(RastPath) for RastPath in AllOutfiles if 'TempProcStack' not in RastPath]
    # Remove None element if there is
    ListOutDates = [i for i in ListOutDates if i]
    
    # Get all unique dates
    UniqueOutDates = list(set(ListOutDates))
    UniqueOutDates.sort()
    
    # test if required date are lower or equal to orthe number of date
    print('\n\n aNumber_Of_Date_To_Keep UniqueOutDates', aNumber_Of_Date_To_Keep, UniqueOutDates)
    if aNumber_Of_Date_To_Keep > len(UniqueOutDates):
        aNumber_Of_Date_To_Keep = UniqueOutDates
    
    print('\n\n aNumber_Of_Date_To_Keep UniqueOutDates', aNumber_Of_Date_To_Keep, UniqueOutDates)
    # get all output surbirectories
    SubFoldersOut = get_immediate_subdirectories(aOutFolder)
    SubFoldersIn = get_immediate_subdirectories(aInFolder)
    
    print('\n\n SubFoldersOut', SubFoldersOut)
    print('\n\n SubFoldersIn', SubFoldersIn)
    # delete zip files
    if aNumber_Of_Date_To_Keep < len(UniqueOutDates):
        NumDateToDel = len(UniqueOutDates) - aNumber_Of_Date_To_Keep
        print('\n\n NumDateToDel', NumDateToDel)
        for date in UniqueOutDates[0:NumDateToDel-1]:
            print('\n\n date',date)
            # list zip files and del if old date
            for folder in SubFoldersIn:
                # del folder
                if date in folder and os.path.exists(folder):
                    print('\nRemove Ortho folder ', folder)
                    shutil.rmtree(folder)
                    
            # list folder and delete if old
            for folder in SubFoldersOut:
                # del folder
                if date in folder and os.path.exists(folder):
                    print('\nRemove Ortho folder ', folder)
                    shutil.rmtree(folder)

def unzip_member_f3(zip_filepath, filename, dest):
    with open(zip_filepath, 'rb') as f:
        zf = zipfile.ZipFile(f)
        zf.extract(filename, dest)
    fn = os.path.join(dest, filename)
    return _count_file(fn)



def f3(fn, dest):
    with open(fn, 'rb') as f:
        zf = zipfile.ZipFile(f)
        futures = []
        with concurrent.futures.ProcessPoolExecutor() as executor:
            for member in zf.infolist():
                futures.append(
                    executor.submit(
                        unzip_member_f3,
                        fn,
                        member.filename,
                        dest,
                    )
                )
            total = 0
            for future in concurrent.futures.as_completed(futures):
                total += future.result()
    return total

def ExtractFullZipFile(aZipPath,aDestPath):
    try:
        archive = zipfile.ZipFile(aZipPath, 'r')
        if archive.testzip() is not None:
            logging.info('Corrupted ZIP ' + aZipPath)
        else:
            archive.extractall(aDestPath)

            archive.close()
            logging.info('File from ZIP well fully extracted :' + aDestPath)

    except  zipfile.BadZipfile:
        logging.info('Bad ZIP ' + aZipPath)
        

def GdalMultilookFactor(aInputFile, aOutputFile, aNodata, aFactor):
    src_ds = gdal.Open( aInputFile )
    if src_ds is None:
        print('Could not open ' + fn)
        # sys.exit(1)
    else:
    # Get raster information
        cols = src_ds.RasterXSize
        rows = src_ds.RasterYSize

        #lecture de la taille de pixel
        cols_out = int(cols / aFactor)
        rows_out = int(rows / aFactor)

		# Multilook
        cmd = "gdalwarp  "
        cmd += "-t_srs EPSG:4326 "
        cmd += "-srcnodata " + str(aNodata)
        cmd += " -dstnodata " + str(aNodata)
        cmd += " -multi "
        cmd += " -r average -of vrt "
        cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
        cmd += aInputFile + " "
        cmd += aOutputFile

        # progress.setInfo('\nGdalMultilookFactor ' + cmd)
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]

    src_ds = None


def DissolveAllFeatures(aInputFile, aOutputFile):
	# ogr2ogr output.shp input.shp -dialect sqlite -sql "SELECT ST_Union(geometry) AS geometry FROM input"

	InputFileName = os.path.basename(os.path.splitext(aInputFile)[0])

	cmd = "ogr2ogr  "
	cmd += aOutputFile + " "
	cmd += aInputFile + " "
	cmd += " -dialect sqlite -sql \"SELECT ST_Union(geometry) AS geometry FROM "
	cmd += InputFileName + "\""

	# progress.setInfo('\nDissolveAllFeatures ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ConvertMLToMask(aInputFile, aOutputFile, aNodata):
	# Multilook
	cmd = "gdal_translate  "
	cmd += " -b mask,1 -of GTiff "
	cmd += "-a_nodata " + str(aNodata) + " "
	cmd += aInputFile + " "
	cmd += aOutputFile

	#print cmd
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ReprojToVRT(aInputFile, aOutputFile, aNodata, aFactor):
    src_ds = gdal.Open( aInputFile )
    if src_ds is None:
        print('Could not open ' + fn)
		# sys.exit(1)
    else:
		# Get raster information
        cols = src_ds.RasterXSize
        rows = src_ds.RasterYSize


		#lecture de la taille de pixel
        cols_out = int(cols / aFactor)
        rows_out = int(rows / aFactor)

        # Multilook
        cmd = "gdalwarp  "
        cmd += "-t_srs EPSG:4326 "
        cmd += "-srcnodata " + str(aNodata)
        cmd += " -dstnodata " + str(aNodata)
        cmd += " -multi "
        cmd += " -r bilinear -of vrt "
        cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
        cmd += aInputFile + " "
        cmd += aOutputFile

		# progress.setInfo('\nReprojToVRT ' + cmd)
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]
	
    src_ds = None

def Polygonize(aInputRaster, aOutputVector):
	# Multilook
	if 'nt' in os.name:
		cmd = "gdal_polygonize.bat   "
	else:
		cmd = "gdal_polygonize.py   "

	cmd += aInputRaster + " "
	cmd += " -mask " + aInputRaster
	cmd += " -b 1 -f \"ESRI Shapefile\" "
	cmd += aOutputVector

	# progress.setInfo('\nPolygonize ' + cmd)
	p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
	ret= p1.communicate()[1]

def ConvexHull(aInputShape, aOutputShape):


	# Get a Layer
	inDriver = ogr.GetDriverByName("ESRI Shapefile")
	inDataSource = inDriver.Open(aInputShape, 0)
	inLayer = inDataSource.GetLayer()

	# prj name of shape
	outPRJfile = aOutputShape.replace('.shp', '.prj')

	#get spatial ref
	spatialRef = inLayer.GetSpatialRef()

	# Collect all Geometry
	geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
	for feature in inLayer:
	    geomcol.AddGeometry(feature.GetGeometryRef())

	# Calculate convex hull
	convexhull = geomcol.ConvexHull()

	# Save extent to a new Shapefile
	outDriver = ogr.GetDriverByName("ESRI Shapefile")

	# Remove output shapefile if it already exists
	if os.path.exists(aOutputShape):
	    outDriver.DeleteDataSource(aOutputShape)

	# Create the output shapefile
	outDataSource = outDriver.CreateDataSource(aOutputShape)
	outLayer = outDataSource.CreateLayer("states_convexhull", geom_type=ogr.wkbPolygon)

	# Add an ID field
	idField = ogr.FieldDefn("id", ogr.OFTInteger)
	outLayer.CreateField(idField)

	# Create the feature and set values
	featureDefn = outLayer.GetLayerDefn()
	feature = ogr.Feature(featureDefn)
	feature.SetGeometry(convexhull)
	feature.SetField("id", 1)
	outLayer.CreateFeature(feature)

	spatialRef.MorphToESRI()
	file = open(outPRJfile, 'w')
	file.write(spatialRef.ExportToWkt())
	file.close()

	feature = None

	# Save and close DataSource
	inDataSource = None
	outDataSource = None

def GdalMultilookFactor(aInputFile, aOutputFile, aNodata, aFactor):
    src_ds = gdal.Open( aInputFile )
    if src_ds is None:
        print('Could not open ' + fn)
		# sys.exit(1)
    else:
		# Get raster information
        cols = src_ds.RasterXSize
        rows = src_ds.RasterYSize

		#lecture de la taille de pixel
        cols_out = int(cols / aFactor)
        rows_out = int(rows / aFactor)

		# Multilook
        cmd = "gdalwarp  "
        cmd += "-t_srs EPSG:4326 "
        cmd += "-srcnodata " + str(aNodata)
        cmd += " -dstnodata " + str(aNodata)
        cmd += " -multi "
        cmd += " -r average -of vrt "
        cmd += " -ts " + str(cols_out) + " " + str(rows_out) + " "
        cmd += aInputFile + " "
        cmd += aOutputFile
        cmd += ' -overwrite '

		#print cmd
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        ret= p1.communicate()[1]

    src_ds = None

def ComputeIntercectionArea(aRefShape,aCompareShape):
	# We open the shape
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(aRefShape, 0)
    layer = dataSource.GetLayer()
    
    FeatLayer = layer[0]
    FeatLayerGeom = FeatLayer.GetGeometryRef()
    
    # We open the shape
    dataSourceComp = driver.Open(aCompareShape, 0)
    layerComp = dataSourceComp.GetLayer()
    
    FeatLayerComp = layerComp[0]
    FeatLayerGeomComp = FeatLayerComp.GetGeometryRef()
    
    # Generate intercection between user polygon and curent S1 footprint
    intersection = FeatLayerGeom.Intersection(FeatLayerGeomComp)
    
    AreaIntercect = intersection.GetArea()
    AreaRef = FeatLayerGeom.GetArea()
    
    PrctArea = AreaIntercect / AreaRef * 100.
    
    # Del all vec element
    FeatLayer = None
    FeatLayerGeom = None
    FeatLayerComp = None
    FeatLayerGeomComp = None
    dataSource = None
    dataSourceComp = None

    
    return  AreaRef, AreaIntercect, PrctArea
    
def TestRasterFootprint(aInputRaster,aRefShape,aPrctThr, aNodata):
	dir_path = os.path.dirname(aInputRaster).replace("\\","/")
	TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	
	# We create TMP dir
	TmpDir = os.path.join(dir_path, "tMp"+ TimeNow).replace("\\","/")
	if not os.path.exists(TmpDir):
		os.makedirs(TmpDir)
		
	InputRasterML = os.path.join(TmpDir, "ml.tif").replace("\\","/")
	InputRasterMLVrt = os.path.join(TmpDir, "mlvrt.vrt").replace("\\","/")
	InputRasterMLVrtTomask = os.path.join(TmpDir, "ml_msk.tif").replace("\\","/")
	InputRasterToShape = os.path.join(TmpDir, "footprint.shp").replace("\\","/")
	InputRasterToShapeDissolve = os.path.join(TmpDir, "footprint_dissolve.shp").replace("\\","/")
	
	#Create ML
	GdalMultilookFactor(aInputRaster, InputRasterML, aNodata, 10)

	# Convert original file to vrt to have nodata
	ReprojToVRT(InputRasterML, InputRasterMLVrt, aNodata, 1)

	# Convert the reproj vrt to mask
	ConvertMLToMask(InputRasterMLVrt, InputRasterMLVrtTomask, aNodata)

	Polygonize(InputRasterMLVrtTomask, InputRasterToShape)

	DissolveAllFeatures(InputRasterToShape, InputRasterToShapeDissolve)



	AreaRef, AreaIntercect, PrctArea = ComputeIntercectionArea(aRefShape,InputRasterToShapeDissolve)
	ValidArea = False
	if PrctArea >= aPrctThr:
		ValidArea = True
		
	# Del Temporay Dir
	shutil.rmtree(TmpDir)
	
	return ValidArea, AreaRef, AreaIntercect, PrctArea
