# -*- coding: utf-8 -*-

"""
/***************************************************************************
 SentinelForAll
                                 A QGIS plugin
 Plugin dedicated to Sentinel data processing
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2020-08-25
        copyright            : (C) 2020 by C. Lardeux / P.L. Frison
        email                : cedric.lardeux@onfinternational.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'C. Lardeux / P.L. Frison'
__date__ = '2020-08-25'
__copyright__ = '(C) 2020 by C. Lardeux / P.L. Frison'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os, subprocess

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterField,
                       QgsProcessingParameterString,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterCrs,
                       QgsProcessingParameterNumber,
                       QgsProcessingOutputFolder,
                       QgsApplication,
                       QgsProcessingParameterDefinition)
from pathlib import Path

import sys

def get_platform():
    platforms = {
        'linux1' : 'Linux',
        'linux2' : 'Linux',
        'darwin' : 'OS X',
        'win32' : 'Windows'
    }
    if sys.platform not in platforms:
        return sys.platform
    
    return platforms[sys.platform]

# platform = get_platform()

# if platform == 'Windows':
#     otb_dir = 'C:\\QGIS_RemoteSensing_64bits\\OTB\\bin'
#     os.environ["PATH"] += os.pathsep + os.pathsep.join([otb_dir])

#     print('\n########\n OS ENV  MAIN PLUNGIN \n', os.environ["PATH"])

class S1OrthoCal(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    '''
python3 /home/frison/A/codes/python/qgis/qgis3/VPython3/S1ProcessByRelativeOrbitFromZip.py -Input_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/zip -DEM_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/DEM -Input_Polygon_File /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/StudyArea/StudyArea.shp -Relative_Orbit_Field_Name RelOrb -Relative_Orbit_To_Process 47 -Calibration_Type Gamma0 -Output_EPSG EPSG:32622 -Output_Resolution 10 -Output_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/OrthoCal -Number_Of_Date_To_Keep 0 -Average_All_Date True -Ram 15000
    '''

    InputDataFolder = 'InputDataFolder'
    DEMFolder = 'DEMFolder'
    InputPolygonFile = 'InputPolygonFile'
    RelativeOrbitFieldName = 'RelativeOrbitFieldName'
    RelativeOrbitToProcess = 'RelativeOrbitToProcess'
    CalibrationType = 'CalibrationType'
    OutputEPSG = 'OutputEPSG'
    OutputResolution = 'OutputResolution'
    OutputDataFolder = 'OutputDataFolder'
    NumberOfDateToKeep = 'NumberOfDateToKeep'
    AverageAllDate = 'AverageAllDate'
    Ram = 'Ram'


    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterFile(
                self.InputDataFolder,
                self.tr('Input folder (containing Sentinel-1 Zip files only):'),
                behavior = 1
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.InputPolygonFile,
                self.tr('Shape file delimitating the study area:')
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                self.RelativeOrbitFieldName,
                self.tr('Field containing the orbit number:'),
                '',
                self.InputPolygonFile
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.RelativeOrbitToProcess,
                self.tr('Orbit relative number to process (ex: 47-120):'),
                defaultValue="47-120"
            )
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.DEMFolder,
                self.tr('Dem folder (containing tif files only):'),
                behavior = 1
            )
        )

        self.addParameter(
            QgsProcessingParameterCrs(
                self.OutputEPSG,
                self.tr('Output EPSG projection (in meter only):'),
                'EPSG:3857'
            )
        )
        
        self.addParameter(
            QgsProcessingParameterNumber(
                self.OutputResolution,
                self.tr('Output pixel size (m):'),
                defaultValue=10.,
                type=QgsProcessingParameterNumber.Double,
            )
        )        


        global calib_list
        calib_list = ['Sigma0', 'Gamma0', 'Beta0', 'sigma' ]

        self.addParameter(
            QgsProcessingParameterEnum(
                self.CalibrationType,
                self.tr('radar coefficient:'),
                options=calib_list,
                allowMultiple=False,
                defaultValue=1
            )
        )

        Num_Date_To_Keep = QgsProcessingParameterNumber(
                self.NumberOfDateToKeep,
                self.tr('Number of acquisitions to keep (0 to keep all the dates):'),
                defaultValue=0.,
                type=QgsProcessingParameterNumber.Integer,
        )
        Num_Date_To_Keep.setFlags(Num_Date_To_Keep.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Num_Date_To_Keep)

        global boolean_list
        boolean_list = ['True', 'False']
        avg_date = QgsProcessingParameterEnum(
                self.AverageAllDate,
                self.tr('Temporal average color composition:'),
                options=boolean_list,
                allowMultiple=False,
                defaultValue=0
        )
        avg_date.setFlags(avg_date.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(avg_date)


        num_ram = QgsProcessingParameterNumber(
                self.Ram,
                self.tr('Processing RAM (Mo):'),
                defaultValue=512,
                type=QgsProcessingParameterNumber.Integer,
            )
        self.addParameter(num_ram)

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OutputDataFolder,
                self.tr('Output folder:'),
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        input_data_folder = self.parameterAsString(parameters,self.InputDataFolder,context)

        dem_folder = self.parameterAsString(parameters,self.DEMFolder,context)

        input_polygon_file = self.parameterDefinition('InputPolygonFile').valueAsPythonString(parameters['InputPolygonFile'], context).replace("\'","")

        relative_orbit_field_name = self.parameterAsString(parameters,self.RelativeOrbitFieldName,context)

        relative_orbit_to_process = self.parameterAsString(parameters,self.RelativeOrbitToProcess,context)

        calibration_type = calib_list[int(self.parameterAsString(parameters,self.CalibrationType,context))]

        output_epsg = self.parameterAsCrs(parameters,self.OutputEPSG,context)
        output_epsg = output_epsg.authid()

        output_resolution = self.parameterAsString(parameters,self.OutputResolution,context)

        output_data_folder = self.parameterAsString(parameters,self.OutputDataFolder,context)

        number_of_date_to_keep = self.parameterAsString(parameters,self.NumberOfDateToKeep,context)

        average_all_date =  boolean_list[int(self.parameterAsString(parameters,self.AverageAllDate,context))]

        ram = self.parameterAsString(parameters,self.Ram,context)


        script_path = Path(__file__).parent
        script_path =  script_path / 'S1ProcessByRelativeOrbitFromZip.py'

        # Load OTB bin to system
        platform = get_platform()
        print('\n##### Platform:\n', platform)

        if platform == 'Windows':
            otb_dir = 'C:\\QGIS_Sentinel\\Remotesensing4AllLibs\\OTB\\bin'
            os.environ["PATH"] += os.pathsep + os.pathsep.join([otb_dir])

            print('\n########\n OS ENV  MAIN PLUNGIN \n', os.environ["PATH"])

        cmd = run_ortho(str(script_path),
                input_data_folder,
                dem_folder,
                input_polygon_file,
                relative_orbit_field_name,
                relative_orbit_to_process,
                calibration_type,
                output_epsg,
                output_resolution,
                output_data_folder,
                number_of_date_to_keep,
                average_all_date,
                ram)

        print('cmd ', cmd)
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        p1.wait()
        ret= p1.communicate()[1]

        # subprocess.check_call(cmd, shell=False)
        # output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stdin=open(os.devnull), stderr=subprocess.STDOUT, universal_newlines=False)
        # output.wait()
        # feedback.pushConsoleInfo(output.communicate()[0].decode("utf-8"))




        return {}

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'S1 Calibration and Orthorectification'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(self.name())

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(self.groupId())

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'S1 Batch Processing'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self, config={}):
        """ Virtual override

            see https://qgis.org/api/classQgsProcessingAlgorithm.html
        """
        return self.__class__()

class S1TemporalFilt(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    '''
python3 /home/frison/A/codes/python/qgis/qgis3/VPython3/S1ProcessByRelativeOrbitFromZip.py -Input_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/zip -DEM_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/DEM -Input_Polygon_File /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/StudyArea/StudyArea.shp -Relative_Orbit_Field_Name RelOrb -Relative_Orbit_To_Process 47 -Calibration_Type Gamma0 -Output_EPSG EPSG:32622 -Output_Resolution 10 -Output_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/OrthoCal -Number_Of_Date_To_Keep 0 -Average_All_Date True -Ram 15000
    '''

    InputDataFolder = 'InputDataFolder'
    WindowSizeForTemporalFiltering = 'WindowSizeForTemporalFiltering'
    AdaptTempChangeThresh = 'AdaptTempChangeThresh'
    ApplyLeePreFilt = 'ApplyLeePreFilt'
    WindowSizeLeeFilt = 'WindowSizeLeeFilt'
    ENLLeeFilt = 'ENLLeeFilt'
    OutputIndB = 'OutputIndB'
    OutputDataFolder = 'OutputDataFolder'
    RelativeOrbitToProcess = 'RelativeOrbitToProcess'
    NumberOfDateToKeep = 'NumberOfDateToKeep'
    Ram = 'Ram'

    TestParam = 'TestParam'


    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.

        self.addParameter(
            QgsProcessingParameterFile(
                self.InputDataFolder,
                self.tr('Input folder (containing Orthorectified files):'),
                behavior = 1
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.RelativeOrbitToProcess,
                self.tr('Relative orbit number to process (ex: 47-120):'),
                defaultValue="47-120"
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.WindowSizeForTemporalFiltering,
                self.tr('Window Size for Temporal iltering:'),
                defaultValue=11,
                type=QgsProcessingParameterNumber.Integer,
            )
        )

        num_ram = QgsProcessingParameterNumber(
                self.Ram,
                self.tr('Processing RAM (Mo):'),
                defaultValue=512.,
                type=QgsProcessingParameterNumber.Integer,
            )
        self.addParameter(num_ram)

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OutputDataFolder,
                self.tr('Output folder:')
            )
        )


        global boolean_list
        boolean_list = ['True', 'False']
        
        Apply_Lee_Pre_Filt = QgsProcessingParameterEnum(
            self.ApplyLeePreFilt,
            self.tr('Apply Lee Pre Filtering:'),
            options=boolean_list,
            allowMultiple=False,
            defaultValue=1
        )
        Apply_Lee_Pre_Filt.setFlags(Apply_Lee_Pre_Filt.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Apply_Lee_Pre_Filt)

        Window_Size_Lee_Filt = QgsProcessingParameterNumber(
            self.WindowSizeLeeFilt,
            self.tr('Window size for Lee Filtering:'),
            defaultValue=5,
            type=QgsProcessingParameterNumber.Integer,
        )
        Window_Size_Lee_Filt.setFlags(Window_Size_Lee_Filt.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Window_Size_Lee_Filt)

        ENL_Lee_Filt = QgsProcessingParameterNumber(
            self.ENLLeeFilt,
            self.tr('ENL for Lee Filtering:'),
            defaultValue=5,
            type=QgsProcessingParameterNumber.Double,
            )
        ENL_Lee_Filt.setFlags(ENL_Lee_Filt.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(ENL_Lee_Filt)


        Adapt_Temp_Change_Thresh = QgsProcessingParameterNumber(
            self.AdaptTempChangeThresh,
            self.tr('Change Threshold for Temporal Filtering (0 for Queegan equivalent):'),
            defaultValue=0.97,
            type=QgsProcessingParameterNumber.Double,
        )
        Adapt_Temp_Change_Thresh.setFlags(Adapt_Temp_Change_Thresh.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Adapt_Temp_Change_Thresh)

        Output_in_dB = QgsProcessingParameterEnum(
            self.OutputIndB,
            self.tr('Output in dB:'),
            options=boolean_list,
            allowMultiple=False,
            defaultValue=0
        )
        Output_in_dB.setFlags(Output_in_dB.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Output_in_dB)
 
        Num_Date_To_Keep = QgsProcessingParameterNumber(
                self.NumberOfDateToKeep,
                self.tr('Number of acquisitions to keep (0 to keep all the dates):'),
                defaultValue=0.,
                type=QgsProcessingParameterNumber.Integer,
        )
        Num_Date_To_Keep.setFlags(Num_Date_To_Keep.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(Num_Date_To_Keep)


    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        input_data_folder = self.parameterAsString(parameters,self.InputDataFolder,context)

        relative_orbit_to_process = self.parameterAsString(parameters,self.RelativeOrbitToProcess,context)

        window_size_for_tempfilt = self.parameterAsString(parameters,self.WindowSizeForTemporalFiltering,context)

        change_threshold_for_tempfit = self.parameterAsString(parameters,self.AdaptTempChangeThresh,context)

        lee_pre_filtering =  boolean_list[int(self.parameterAsString(parameters,self.ApplyLeePreFilt,context))]

        window_size_lee_filtering = self.parameterAsString(parameters,self.WindowSizeLeeFilt,context)

        enl_lee_filtering = self.parameterAsString(parameters,self.ENLLeeFilt,context)

        output_db =  boolean_list[int(self.parameterAsString(parameters,self.OutputIndB,context))]

        output_data_folder = self.parameterAsString(parameters,self.OutputDataFolder,context)

        number_of_date_to_keep = self.parameterAsString(parameters,self.NumberOfDateToKeep,context)

        ram = self.parameterAsString(parameters,self.Ram,context)


        script_path = Path(__file__).parent
        script_path =  script_path / 'S1AdaptTemporalFiltering.py'

        # Load OTB bin to system
        platform = get_platform()
        print('\n##### Platform:\n', platform)

        if platform == 'Windows':
            otb_dir = 'C:\\QGIS_Sentinel\\Remotesensing4AllLibs\\OTB\\bin'
            os.environ["PATH"] += os.pathsep + os.pathsep.join([otb_dir])

        cmd = run_tempfilt(str(script_path),
                input_data_folder,
                window_size_for_tempfilt,
                change_threshold_for_tempfit,
                lee_pre_filtering,
                window_size_lee_filtering,
                enl_lee_filtering,
                output_db,
                output_data_folder,
                relative_orbit_to_process,
                number_of_date_to_keep,
                ram)


        print('cmd ', cmd)
        p1 = subprocess.Popen (cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        p1.wait()
        ret= p1.communicate()[1]

        # subprocess.check_call(cmd, shell=False)
        # output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stdin=open(os.devnull), stderr=subprocess.STDOUT, universal_newlines=False)
        # output.wait()
        # feedback.pushConsoleInfo(output.communicate()[0].decode("utf-8"))




        return {}

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'S1 Temporal Filtering'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr(self.name())

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr(self.groupId())

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'S1 Batch Processing'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self, config={}):
        """ Virtual override

            see https://qgis.org/api/classQgsProcessingAlgorithm.html
        """
        return self.__class__()


def run_ortho(script_path,
                input_data_folder,
                dem_folder,
                input_polygon_file,
                relative_orbit_field_name,
                relative_orbit_to_process,
                calibration_type,
                output_epsg,
                output_resolution,
                output_data_folder,
                number_of_date_to_keep,
                average_all_date,
                ram):
    cmd = "python3 "
    cmd += script_path
    cmd += ' -Input_Data_Folder ' + input_data_folder
    cmd += ' -DEM_Folder ' + dem_folder
    cmd += ' -Input_Polygon_File ' + input_polygon_file
    cmd += ' -Relative_Orbit_Field_Name ' + relative_orbit_field_name
    cmd += ' -Relative_Orbit_To_Process ' + str(relative_orbit_to_process)
    cmd += ' -Calibration_Type ' + calibration_type
    cmd += ' -Output_EPSG ' + output_epsg
    cmd += ' -Output_Resolution ' + str(output_resolution)
    cmd += ' -Output_Data_Folder ' + output_data_folder
    cmd += ' -Number_Of_Date_To_Keep ' + str(number_of_date_to_keep)
    cmd += ' -Average_All_Date ' + str(average_all_date)
    cmd += ' -Ram ' + str(ram)

    

    return cmd

def run_tempfilt(script_path,
                input_data_folder,
                window_size_for_tempfilt,
                change_thresold_for_tempfilt,
                lee_pre_filtering,
                window_size_lee_filtering,
                enl_lee_filtering,
                output_db,
                output_data_folder,
                relative_orbit_to_process,
                number_of_date_to_keep,
                ram):

    cmd = "python3 "
    cmd += script_path
    cmd += ' -InDir ' + input_data_folder
    cmd += ' -WinTempFilt ' + str(window_size_for_tempfilt)
    cmd += ' -AdaptTempChangeThresch ' + str(change_thresold_for_tempfilt)
    cmd += ' -ApplyLeePreFilt ' + str(lee_pre_filtering)
    cmd += ' -WinLeeFilt ' + str(window_size_lee_filtering)
    cmd += ' -ENLLeeFilt ' + str(enl_lee_filtering)
    cmd += ' -OutputIndB ' + str(output_db)
    cmd += ' -OutDir ' + output_data_folder
    cmd += ' -RelativeOrbit ' + str(relative_orbit_to_process)
    cmd += ' -NumDateToKeep ' + str(number_of_date_to_keep)
    cmd += ' -Ram ' + str(ram)

    

    return cmd



'''
Package to install
sudo apt install otb             otb-bin         otb-bin-qt      otb-i18n        otb-qgis python3-scipy


wget https://github.com/ubarsc/rios/releases/download/rios-1.4.10/rios-1.4.10.tar.gz && \
         tar -xzf rios-1.4.10.tar.gz && \
         mkdir RIOSInstall && cd rios-1.4.10 && \
         python3 setup.py install --prefix=../RIOSInstall && \
         mv ../RIOSInstall /usr/local/lib

# Instal sentinelsat python lib
pip install --upgrade pip
pip install sentinelsat

# Add env var
touch /etc/profile.d/AddPath.sh  && \
chmod +x /etc/profile.d/AddPath.sh && \
cd -

echo "if [ -d \"/usr/local/lib/RIOSInstall/bin\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    PATH=\"\$PATH:/usr/local/lib/RIOSInstall/bin\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh && \
echo "if [ -d \"/usr/local/lib/RIOSInstall/lib/python3.8/site-packages\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    export PYTHONPATH=\"/usr/local/lib/RIOSInstall/lib/python3.8/site-packages\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh

'''