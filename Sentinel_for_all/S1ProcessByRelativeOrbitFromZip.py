# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 18:14:51 2017

@author: cedric
"""

'''
This script apply calibration and orthorectification process of S1 GRD data
'''

'''
IMPORT
'''
import os, sys
from inspect import getsourcefile
import logging
from datetime import datetime
import shutil

from argparse import ArgumentParser



'''
This function join path always using unix sep
'''
def modOsJoinPath(alistToJoin):
    joinedPath = os.path.join(*alistToJoin).replace("\\","/")

    return joinedPath


'''
This function return the qgis script folder
'''
def getQGISProcessingScriptFolder():
    from qgis.core import QgsApplication
    QGISProcessingScriptFolder = os.path.dirname(QgsApplication.qgisSettingsDirPath())
    QGISProcessingScriptFolder = modOsJoinPath([QGISProcessingScriptFolder,
    'processing', 'scripts'])

    return QGISProcessingScriptFolder

'''
This function load the necessary libs in different way
if we are running script in qgis processing or not
'''
def AddS1LibToPath():
    # Boolean to know if in qgis
    try:
        import qgis.utils
        inqgis = qgis.utils.iface is not None
    except ImportError:
        no_requests = True
        inqgis = False

    if inqgis:
        QGISProcessingScriptFolder = getQGISProcessingScriptFolder()

        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([QGISProcessingScriptFolder, 'S1Lib'])
    else:
        LocalScriptFileDir = os.path.dirname(os.path.abspath((getsourcefile(lambda:0)))).replace("\\","/")
        # Create the S1Lib lib folder path
        ScriptPath = modOsJoinPath([LocalScriptFileDir, 'S1Lib'])

    # Add path to sys
    sys.path.insert(0,ScriptPath)



# Load OTB Libs
AddS1LibToPath()

# Load the libs
from S1OwnLib import (ReturnRealCalibrationOTBValue,
                          GetFileByExtensionFromDirectory,
                          GetNewDatesFromListZipFilesInput,
                          ReprojVector,
                          CheckAllDifferentRelativeOrbit,
                          getS1ZipByTile,
                          CreateShapeFromPath,
                          ProcessS1ZipDataset,
                          GenerateTemporalAverage,
                          RemoveOldestZipAndOrho,
                          TestRasterFootprint,
                          ValidShapeFile,
                          CreateShapeOfAllData,
                          DissolveField,
                          getZipInterectPolyg)
'''
NOTES
Things to do
    - Optimize the ortorectification by using extent of the polygon to replace clip
python3 /home/frison/A/codes/python/qgis/qgis3/VPython3/S1ProcessByRelativeOrbitFromZip.py -Input_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/zip -DEM_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/DEM -Input_Polygon_File /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/StudyArea/StudyArea.shp -Relative_Orbit_Field_Name RelOrb -Relative_Orbit_To_Process 47 -Calibration_Type Gamma0 -Output_EPSG EPSG:32622 -Output_Resolution 10 -Output_Data_Folder /home/frison/A/data/TP/Training/Guyane/ESA_Academy/data/S1/OrthoCal -Number_Of_Date_To_Keep 0 -Average_All_Date True -Ram 15000
'''

def main():
    parser = ArgumentParser()
    
    parser.add_argument('-Input_Data_Folder',type=str, help="Give the folder containing all the S1 data in zip format", dest='Input_Data_Folder')
    parser.add_argument('-DEM_Folder',type=str, help="Give the folder containing the SRTM 1sec arc in epsg 4326", dest='DEM_Folder')
    parser.add_argument('-Input_Polygon_File',type=str, help="Give the shape file containing the study area polygons (one per relative orbit)", dest='Input_Polygon_File')
    parser.add_argument('-Relative_Orbit_Field_Name',type=str, help="Give the field name that contain the relative orbit to process for each polygon of your shape", dest='Relative_Orbit_Field_Name')
    parser.add_argument('-Relative_Orbit_To_Process',type=str, help="Give the relative orbit to process like 112-39 to process orbite 112 and 39", dest='Relative_Orbit_To_Process')
    parser.add_argument('-Calibration_Type',type=str, help="Give the calibration type like Gamma0 or Sigma0", dest='Calibration_Type')
    parser.add_argument('-Output_EPSG',type=str, help="Give the output projection that MUST be in meter !!! like EPSG:32621", dest='Output_EPSG')
    parser.add_argument('-Output_Resolution',type=str, default = 10, help="Give the output resolution in meter", dest='Output_Resolution')
    parser.add_argument('-Output_Data_Folder',type=str, help="Give the output data folder which will contain all the output.", dest='Output_Data_Folder')
    parser.add_argument('-Number_Of_Date_To_Keep',type=int, help="Give the number of date to keep after processing to save space. If you put 0 it will not remove data", dest='Number_Of_Date_To_Keep')
    parser.add_argument('-Average_All_Date',type=str, help="Enable to generate a new output that is the average of all timeserie in the output folder, True or False", dest='Average_All_Date')
    parser.add_argument('-Ram',type=str, help="Give the available ram that you want to allocate", dest='Ram')

    #~ help="python /home/cedric/Git/UpdFoss4GFRDev/S1ProcessByRelativeOrbitFromZip.py -Input_Data_Folder /media/cedric/CL/Cotriguacu/CL/TestDLCode -DEM_Folder  /media/cedric/CL/Cotriguacu/CL/DEM -Input_Polygon_File /media/cedric/CL/Cotriguacu/CL/InputZip/CotriguacuEPSG32721.shp -Relative_Orbit_Field_Name id  -Relative_Orbit_To_Process 112-39-54 -Calibration_Type Gamma0 -Output_EPSG EPSG:32621 -Output_Resolution 10 -Output_Data_Folder /media/cedric/CL/Cotriguacu/CL/Ortho -Number_Of_Date_To_Keep 0 -Average_All_Date True -Ram 5000")
    
    
    #~ parser.print_help()
    args = parser.parse_args()
    
    Input_Data_Folder = args.Input_Data_Folder
    DEM_Folder = args.DEM_Folder
    Input_Polygon_File = args.Input_Polygon_File
    Relative_Orbit_Field_Name = args.Relative_Orbit_Field_Name
    Relative_Orbit_To_Process = args.Relative_Orbit_To_Process
    Calibration_Type = args.Calibration_Type
    Output_EPSG =args.Output_EPSG
    Output_Resolution = args.Output_Resolution
    Output_Data_Folder = args.Output_Data_Folder
    Number_Of_Date_To_Keep = args.Number_Of_Date_To_Keep
    Average_All_Date =  args.Average_All_Date
    Ram = args.Ram
    
    RunProcessing(Input_Data_Folder, \
                  DEM_Folder, \
                  Input_Polygon_File, \
                  Relative_Orbit_Field_Name, \
                  Relative_Orbit_To_Process, \
                  Calibration_Type,\
                  Output_EPSG,\
                  Output_Resolution, \
                  Output_Data_Folder,\
                  Number_Of_Date_To_Keep,  \
                  Average_All_Date,\
                  Ram)


def RunProcessing(Input_Data_Folder, \
                  DEM_Folder, \
                  Input_Polygon_File, \
                  Relative_Orbit_Field_Name, \
                  Relative_Orbit_To_Process, \
                  Calibration_Type,\
                  Output_EPSG,\
                  Output_Resolution, \
                  Output_Data_Folder,\
                  Number_Of_Date_To_Keep,  \
                  Average_All_Date,\
                  Ram):
    
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    '''
    Input OF THE PROGRAM
    '''

    '''
    THE PROGRAM ITSELF
    '''
    # Internal variable
    Noise = 'false'
    '''
    Main step
    1 - Transform Calibration user input into OTB command and get its acronym
    2 - List ALL Sentinel-1 Manifest file
    3 - Filter the one that not already processed in the output folder
    4 - Scan path for all polygon and control that there is different path for all
    polygon
    5 - Loop thru relative orbit given by the user and
        5a - Loop thru polygon
            For each relative orbit loop thru all the polygon of this orbit
            - Create on subfolder per polygon
            - Filter all the data to take the one which intersect the study area
            - Process the data
    '''
    
    # Logging file parameters
    TimeNow = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    
    LogFile =  modOsJoinPath([Output_Data_Folder, 'LogFile-OrthoByPath_' + TimeNow + '.txt'])
    logging.basicConfig(filename=LogFile,level=logging.INFO)
    
    logging.info('Begin the process')
    
    
    
    '''
    - List zip files in input and filter comparing output date do get the new one
    - 
    '''
    global Nodata
    global PrctThr
    Nodata = 0.

    PrctThr = 98. 
    # date field for footprint data shape file
    DateField = 'date'
    
    
    
    # 1 - Transform Calibration user input into OTB command and get its acronym
    OTBCalibrationType, CalibrationName = ReturnRealCalibrationOTBValue(Calibration_Type)
    
    # 2 - List ALL Sentinel-1 Manifest file
    
    # 4 - Scan path for all polygon and control that there is different path for all
    # polygon
    # Get Path name list user
    PathUserList = Relative_Orbit_To_Process.split('-')
    PathUserList = [int(path) for path in PathUserList]

    # We create TMP dir
    # We get File dir
    dir_path = os.path.dirname(os.path.realpath(Input_Polygon_File)).replace("\\","/")
    TmpDir = os.path.join(dir_path, "tMp" + TimeNow).replace("\\","/")
    if not os.path.exists(TmpDir):
        os.makedirs(TmpDir)
    
    
    # Reproject Shapefile to 4326 to be compliant with S1 raw data
    Input_Polygon_FileEPSG4326 = Input_Polygon_File.replace('.shp', 'EPSG4326.shp')
    ReprojVector(Input_Polygon_File, Input_Polygon_FileEPSG4326, 4326)

    # fix the geometry of projected file
    ValidShapeFile(Input_Polygon_FileEPSG4326)

    CheckAllDifferentRelativeOrbit(Input_Polygon_FileEPSG4326,Relative_Orbit_Field_Name, PathUserList)

    # List all SENTINEL-1 manifest.safe files
    ZipFiles = GetFileByExtensionFromDirectory(Input_Data_Folder, '.zip')

    # Create the shape of all data footprint including orbit and date field
    ShapeFootprint = os.path.join(TmpDir, "footprint.shp").replace("\\","/")
    CreateShapeOfAllData(ZipFiles, ShapeFootprint, DateField, Relative_Orbit_Field_Name)

    # Dissolve the result using date field
    ShapeFootprintDissolve = os.path.join(TmpDir, "footprintDissolve.shp").replace("\\","/")
    DissolveField(ShapeFootprint, ShapeFootprintDissolve, DateField)
    
    
    # 5 - Loop thru relative orbit given by the user and
    #    5a - Loop thru polygon
    for userPath in PathUserList:
        logging.info('Process tile ' + str(userPath) + ' From ' + '-'.join(str(PathUserList)))
        PathDir = modOsJoinPath([Output_Data_Folder, 'p' + str(userPath)])

        





        # List all SENTINEL-1 manifest.safe files
        ZipFiles = GetFileByExtensionFromDirectory(Input_Data_Folder, '.zip')
        # check if wrong download file is not present searching .aria2 file
        Aria2Files =  GetFileByExtensionFromDirectory(Input_Data_Folder, '.aria2')
        Aria2Files = [os.path.basename(os.path.splitext(A2file)[0]) for A2file in Aria2Files ]
        
        if len(Aria2Files) > 0:
            ZipFiles = [Zipfile for Zipfile in ZipFiles if not os.path.basename(Zipfile) in Aria2Files]
    
        if len(ZipFiles) == 0:
            logging.info('There is no valid data to process for the current tile we continue to next tile')
            continue
        
        # 3 - Filter the one that not already processed in the output folder
        ZipFiles = GetNewDatesFromListZipFilesInput(ZipFiles, Input_Data_Folder, PathDir)
        
        # Select zip that alow to cover at lis x% of the study area
        intersectRaster = getZipInterectPolyg(Input_Polygon_FileEPSG4326,ShapeFootprintDissolve,ZipFiles, Relative_Orbit_Field_Name, userPath, DateField, PrctThr)


        # If intersect is not empty we create output directory
        if len(intersectRaster) > 0:
            if not os.path.exists(PathDir):
                os.makedirs(PathDir)
        else: # if no data go to next path
            continue
    
        # Create Shape file of the current path
        PathShape = modOsJoinPath([PathDir, 'p' + str(userPath) + '.shp'])
        CreateShapeFromPath(Input_Polygon_FileEPSG4326,
                            Relative_Orbit_Field_Name,
                            userPath,
                            PathShape)
    
        # Run the process
        ProcessS1ZipDataset(intersectRaster,
                         PathDir,
                         PathShape,
                         DEM_Folder,
                         Output_Resolution,
                         Calibration_Type,
                         Noise,
                         CalibrationName,
                         OTBCalibrationType,
                         Output_EPSG,
                         PrctThr,
                         Ram)
    	# Average all data if enable
        GeomFiles=GetFileByExtensionFromDirectory(PathDir, '.geom')
        for GFile in GeomFiles:
            if os.path.exists(GFile):
                os.remove(GFile)

        if Average_All_Date=="True":
            GenerateTemporalAverage(PathDir, Ram, Output_EPSG)

        # Scan all input and outpufiles to remove oldest one
        if Number_Of_Date_To_Keep > 0:
            RemoveOldestZipAndOrho(Number_Of_Date_To_Keep, Input_Data_Folder, PathDir)

    shutil.rmtree(TmpDir)
    
if __name__ == '__main__':
    main()
