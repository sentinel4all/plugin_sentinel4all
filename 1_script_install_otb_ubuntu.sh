# Download and instal OTB-7.2
wget https://www.orfeo-toolbox.org/packages/archives/OTB/OTB-8.1.0-Linux64.run

chmod +x OTB-8.1.0-Linux64.run && \
mv OTB-8.1.0-Linux64.run /usr/local/lib && \
cd /usr/local/lib && \
./OTB-8.1.0-Linux64.run && \
rm OTB-8.1.0-Linux64.run && \

# Add env var
touch /etc/profile.d/AddPath.sh  && \
chmod +x /etc/profile.d/AddPath.sh && \
cd -
echo "if [ -d \"/usr/local/lib/OTB-8.1.0-Linux64/bin\" ] ; then" >> /etc/profile.d/AddPath.sh && \
echo "    PATH=\"\$PATH:/usr/local/lib/OTB-8.1.0-Linux64/bin\"" >> /etc/profile.d/AddPath.sh && \
echo "fi" >> /etc/profile.d/AddPath.sh
